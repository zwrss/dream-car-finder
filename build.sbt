name := "dream-car-finder"

organization := "com.github.zwrss"

version := "0.1"

scalaVersion := "2.12.5"

libraryDependencies += "org.scala-lang.modules" %% "scala-xml" % "1.1.0"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.9"

libraryDependencies += "net.htmlparser.jericho" % "jericho-html" % "3.4"

libraryDependencies += "org.asynchttpclient" % "async-http-client" % "2.4.5"

libraryDependencies += "com.h2database" % "h2" % "1.4.197"

libraryDependencies += "org.scala-lang.modules" %% "scala-parser-combinators" % "1.0.4"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.1.7"

enablePlugins(JavaAppPackaging)

Keys.mainClass in (Compile) := Some("com.github.zwrss.car.App")
