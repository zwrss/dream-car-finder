package com.github.zwrss.html

import net.htmlparser.jericho.Element

import scala.collection.JavaConversions.`deprecated collectionAsScalaIterable`

sealed abstract class HtmlSelector {
  private[zwrss] def isSatisfiedBy(element: Element): Boolean = _isSatisfiedBy(element)

  protected def _isSatisfiedBy(element: Element): Boolean

  def &&(that: HtmlSelector): HtmlSelector = new AndSelector(List(this, that))

  def ||(that: HtmlSelector): HtmlSelector = new OrSelector(List(this, that))
}

class TagNameSelector(name: String) extends HtmlSelector {
  override protected def _isSatisfiedBy(element: Element): Boolean = element.getName == name
}

class AttributeValueSelector(attribute: String, value: String) extends HtmlSelector {
  override protected def _isSatisfiedBy(element: Element): Boolean = {
    val classValue = Option(element.getAttributeValue(attribute))
    classValue exists (_ split ' ' contains value)
  }
}

class AndSelector(selectors: List[HtmlSelector]) extends HtmlSelector {
  override protected def _isSatisfiedBy(element: Element): Boolean = {
    selectors.forall(_ isSatisfiedBy element)
  }
}

object AndSelector {

  implicit class SelectorExtender(val selector: HtmlSelector) extends AnyVal {
    def and(other: HtmlSelector): AndSelector = HtmlSelector.and(selector, other)
  }

}

class OrSelector(selectors: List[HtmlSelector]) extends HtmlSelector {
  override protected def _isSatisfiedBy(element: Element): Boolean = {
    selectors.exists(_ isSatisfiedBy element)
  }
}

object OrSelector {

  implicit class SelectorExtender(val selector: HtmlSelector) extends AnyVal {
    def or(other: HtmlSelector): OrSelector = HtmlSelector.or(selector, other)
  }

}

class HasChildSelector(selector: HtmlSelector) extends HtmlSelector {
  override protected def _isSatisfiedBy(element: Element): Boolean = {
    element.getChildElements exists selector.isSatisfiedBy
  }
}

class HasValueSelector(regex: String) extends HtmlSelector {
  override protected def _isSatisfiedBy(element: Element): Boolean = {
    regex.r.findFirstMatchIn(element.getContent.toString).isDefined
  }
}

class NotSelector(inner: HtmlSelector) extends HtmlSelector {
  override protected def _isSatisfiedBy(element: Element): Boolean = !(inner isSatisfiedBy element)
}

class ChildNoSelector(i: Int) extends HtmlSelector {
  override protected def _isSatisfiedBy(element: Element): Boolean = ???
}

object HtmlSelector {
  implicit def tagName(name: String): HtmlSelector = new TagNameSelector(name)

  def hasClass(name: String): HtmlSelector = new AttributeValueSelector("class", name)

  def hasID(name: String): HtmlSelector = new AttributeValueSelector("id", name)

  def hasAttribute(attribute: String, value: String): HtmlSelector = new AttributeValueSelector(attribute, value)

  def and(selectors: HtmlSelector*) = new AndSelector(selectors.toList)

  def or(selectors: HtmlSelector*) = new OrSelector(selectors.toList)

  def hasChild(selector: HtmlSelector) = new HasChildSelector(selector)

  def hasValue(value: String) = new HasValueSelector(value)

  def not(selector: HtmlSelector) = new NotSelector(selector)
}
