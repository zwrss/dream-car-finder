package com.github.zwrss.html

import net.htmlparser.jericho.Element

sealed abstract class HtmlExtractor[T] {
  private[zwrss] def _extract(element: Element): Option[T] = extract(element)
  protected def extract(element: Element): Option[T]
}

object AsHtmlExtractor extends HtmlExtractor[String] {
  override protected def extract(element: Element): Option[String] = Option(element.toString)
}

object TagNameExtractor extends HtmlExtractor[String] {
  override protected def extract(element: Element): Option[String] = Option(element.getName)
}

object ValueExtractor extends HtmlExtractor[String] {
  override protected def extract(element: Element): Option[String] = Option(element.getContent.toString.trim)
}

object CleanValueExtractor extends HtmlExtractor[String] {
  override protected def extract(element: Element): Option[String] = {
    Option(element.getContent.getTextExtractor.toString.trim)
  }
}

class AttributeValueExtractor(attribute: String) extends HtmlExtractor[String] {
  override protected def extract(element: Element): Option[String] = Option(element getAttributeValue attribute)
}

object AsLookupExtractor extends HtmlExtractor[HtmlLookup] {
  override protected def extract(element: Element): Option[HtmlLookup] = Option(HtmlLookup(List(element)))
}

object HtmlExtractor {
  def asHtml: HtmlExtractor[String] = AsHtmlExtractor
  def asTagName: HtmlExtractor[String] = TagNameExtractor
  def asAttributeValue(attribute: String): HtmlExtractor[String] = new AttributeValueExtractor(attribute)
  def asHref: HtmlExtractor[String] = asAttributeValue("href")
  def asValue: HtmlExtractor[String] = ValueExtractor
  def asCleanValue: HtmlExtractor[String] = CleanValueExtractor
  def asLookup: HtmlExtractor[HtmlLookup] = AsLookupExtractor
}
