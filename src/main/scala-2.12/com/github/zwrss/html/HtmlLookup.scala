package com.github.zwrss.html

import java.io.ByteArrayInputStream
import java.io.InputStream

import net.htmlparser.jericho.Element
import net.htmlparser.jericho.Source

import scala.collection.convert.ImplicitConversionsToScala._

sealed abstract class HtmlLookup {

  protected def elements: List[Element]

  final def \(idx: Int): HtmlLookup = {
    if (idx < elements.size) SingleHtmlLookup(elements(idx)) else EmptyHtmlLookup
  }

  // find child
  final def \(selector: HtmlSelector): HtmlLookup = {
    val matchingChildren = elements.flatMap(_.getChildElements).collect {
      case c if selector isSatisfiedBy c => c
    }
    HtmlLookup(matchingChildren)
  }

  // find recursive
  final def \\(selector: HtmlSelector): HtmlLookup = {
    def f(element: Element): List[Element] = {
      if (selector isSatisfiedBy element) List(element)
      else element.getChildElements.toList flatMap f
    }
    val matchingChildren = elements.flatMap(_.getChildElements) flatMap f
    HtmlLookup(matchingChildren)
  }

  // find first
  final def ?(selector: HtmlSelector): HtmlLookup = {
    HtmlLookup(\(selector).elements.headOption.toList)
  }

  // find first recursive
  final def ??(selector: HtmlSelector): HtmlLookup = {
    HtmlLookup(\\(selector).elements.headOption.toList)
  }

  // find child or this
  final def \?(selector: HtmlSelector): HtmlLookup = \(selector) match {
    case EmptyHtmlLookup => this
    case x               => x
  }

  // exrtact list
  final def >>[T](extractor: HtmlExtractor[T]): List[T] = elements flatMap extractor._extract

  // extract opt
  final def >?[T](extractor: HtmlExtractor[T]): Option[T] = elements match {
    case head :: Nil => extractor _extract head
    case _ => None
  }

  // extract single
  final def >[T](extractor: HtmlExtractor[T]): T = >?(extractor).get

  final def map(f: HtmlLookup => HtmlLookup): HtmlLookup = {
    MultipleHtmlLookup(elements.flatMap { e =>
      f(SingleHtmlLookup(e)) match {
        case MultipleHtmlLookup(es) => es
        case SingleHtmlLookup(e) => List(e)
        case _ => Nil
      }
    })
  }

  final def first: HtmlLookup = HtmlLookup(elements.headOption.toList)

  final def last: HtmlLookup = HtmlLookup(elements.lastOption.toList)

  override def hashCode(): Int = elements.mkString.hashCode

  override def equals(obj: scala.Any): Boolean = obj match {
    case s: HtmlLookup => s.elements.mkString == elements.mkString
    case _             => false
  }

}

object HtmlLookup {

  def apply(inputStream: InputStream): HtmlLookup = {
    apply(new Source(inputStream).getChildElements.toList)
  }

  def apply(html: String): HtmlLookup = {
    apply(new ByteArrayInputStream(html.getBytes))
  }

  def list(html: String*): HtmlLookup = {
    HtmlLookup(html mkString "")
  }

  def apply(nodes: List[Element]): HtmlLookup = nodes match {
    case Nil           => EmptyHtmlLookup
    case (head :: Nil) => SingleHtmlLookup(head)
    case list          => MultipleHtmlLookup(list)
  }

  def empty: HtmlLookup = EmptyHtmlLookup

}

case class SingleHtmlLookup(element: Element) extends HtmlLookup {

  override protected def elements: List[Element] = List(element)

}

case class MultipleHtmlLookup(elements: List[Element]) extends HtmlLookup

case object EmptyHtmlLookup extends HtmlLookup {
  override protected def elements: List[Element] = List.empty
}