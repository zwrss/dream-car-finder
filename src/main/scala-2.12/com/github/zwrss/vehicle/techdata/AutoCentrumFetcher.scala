package com.github.zwrss.vehicle.techdata

import java.io.File

import com.github.zwrss.cache.FileCache
import com.github.zwrss.html.HtmlLookup
import com.github.zwrss.http.HttpClient
import com.github.zwrss.http.HttpResponse
import com.github.zwrss.http.asynchttpclient.AsyncHttpClientWrapper
import com.github.zwrss.http.caching.CachingHttpClient
import com.github.zwrss.http.delaying.DelayingHttpClient

import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.DurationLong
import scala.concurrent.duration.Duration
import com.github.zwrss.html.HtmlExtractor._
import com.github.zwrss.html.HtmlLookup
import com.github.zwrss.html.HtmlSelector._
import com.github.zwrss.otomoto.mapper.OtomotoParametersMapping
import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue

import scala.concurrent.ExecutionContext.Implicits.global



class AutoCentrumFetcher(http: HttpClient) {

  private val baseUrl: String = "https://www.autocentrum.pl"

  private implicit def toNode(s: String): HtmlLookup = HtmlLookup apply s

  def getMakes: Future[List[String]] = {
    val urlPrefix = "/dane-techniczne/"
    http.get(baseUrl + urlPrefix).map { response =>
      val responseHtml: HtmlLookup = response.getResponseBody
      (responseHtml \\ hasClass("make") >> asAttributeValue("href")).map { link =>
        link.stripPrefix(urlPrefix).stripSuffix("/")
      }
    }
  }

  def getModels(make: String): Future[List[String]] = {
    val urlPrefix = s"/dane-techniczne/$make/"
    http.get(baseUrl + urlPrefix).map { response =>
      val responseHtml: HtmlLookup = response.getResponseBody
      (responseHtml \\ hasClass("car-selector-box") >> asAttributeValue("href")).map { link =>
        link.stripPrefix(urlPrefix).stripSuffix("/")
      }
    }
  }

  def getGenre(make: String, model: String): Future[List[String]] = {
    val urlPrefix = s"/dane-techniczne/$make/$model/"
    http.get(baseUrl + urlPrefix).map { response =>
      val responseHtml: HtmlLookup = response.getResponseBody
      (responseHtml \\ hasClass("car-selector-box") >> asAttributeValue("href")).map { link =>
        link.stripPrefix(urlPrefix).stripSuffix("/")
      }
    }
  }

  def getVersions(make: String, model: String, genre: String): Future[List[String]] = {
    val urlPrefix = s"/dane-techniczne/$make/$model/$genre/"
    http.get(baseUrl + urlPrefix).map { response =>
      val responseHtml: HtmlLookup = response.getResponseBody
      (responseHtml \\ hasClass("car-selector-box") >> asAttributeValue("href")).map { link =>
        link.stripPrefix(urlPrefix).stripSuffix("/")
      }
    }
  }

  def getData(make: String, model: String, genre: Option[String], version: Option[String]): Future[List[(String, String)]] = {
    val urlPrefix = s"/dane-techniczne/$make/$model/" + genre.map(_ + "/").getOrElse("")
    http.get(baseUrl + urlPrefix).map { response =>
      val responseHtml: HtmlLookup = response.getResponseBody
      (responseHtml \\ hasClass("dt-row") >> asLookup).map { node =>
        val attribute = node
        ???
      }
    }
  }

}

object AutoCentrumFetcher {

  private lazy val httpClient = new CachingHttpClient(
    new DelayingHttpClient(
      AsyncHttpClientWrapper.default,
      100.millis
    ),
    new FileCache[String, HttpResponse](new File("cache/http"))
  )

  private implicit def sync[T](future: Future[T]): T = Await.result(future, Duration.Inf)

  def main(args: Array[String]): Unit = {
    val fetcher = new AutoCentrumFetcher(httpClient)

    val makes: List[String] = fetcher.getMakes
    makes.foreach { make =>
      println(make)
      val models: List[String] = fetcher.getModels(make)
      models.foreach { model =>
        println("\t" + model)
        val genres: List[String] = fetcher.getGenre(make, model)
        genres.foreach { genre =>
          println("\t\t" + genre)
          val versions: List[String] = fetcher.getVersions(make, model, genre)
          versions.foreach { version =>
            println("\t\t\t" + version)
          }
        }
      }
    }

    httpClient.close()
  }
}