package com.github.zwrss.vehicle.techdata

import java.io.File

import com.github.zwrss.cache.FileCache
import com.github.zwrss.html.HtmlExtractor._
import com.github.zwrss.html.HtmlLookup
import com.github.zwrss.html.HtmlSelector._
import com.github.zwrss.http.HttpClient
import com.github.zwrss.http.HttpResponse
import com.github.zwrss.http.asynchttpclient.AsyncHttpClientWrapper
import com.github.zwrss.http.caching.CachingHttpClient
import com.github.zwrss.http.delaying.DelayingHttpClient
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.Json

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.concurrent.duration.DurationInt

class CarsDataFetcher(http: HttpClient) {

  private val baseUrl: String = "https://www.cars-data.com/en/"

  private implicit def toNode(s: String): HtmlLookup = HtmlLookup apply s

  def getData(url: String): Future[JsValue] = {
    http.get(baseUrl + url).map { response =>
      val html: HtmlLookup = response.getResponseBody

      val title: String = (html \\ hasClass("title") > asCleanValue).stripSuffix("specs").trim

      val elems: Seq[(String, JsObject)] = {
        html \\ (hasClass("row") && hasClass("box")) >> asLookup map { elem =>
          val title = elem \ "h2" > asCleanValue
          val propertyKeys = (elem \ "dt" >> asCleanValue).map(_.stripSuffix(":").trim)
          val propertyValues = (elem \ "dd" >> asCleanValue).map(x => JsString(x.trim))
          title -> JsObject(propertyKeys zip propertyValues)
        }
      }


      JsObject(
        Seq("CAR" -> JsString(title)) ++ elems
      )
    }
  }

}

object CarsDataFetcher {

  private lazy val httpClient = new CachingHttpClient(
    new DelayingHttpClient(
      AsyncHttpClientWrapper.default,
      100.millis
    ),
    new FileCache[String, HttpResponse](new File("cache/http"))
  )

  private implicit def sync[T](future: Future[T]): T = Await.result(future, Duration.Inf)

  def main(args: Array[String]): Unit = {
    val fetcher = new CarsDataFetcher(httpClient)

    val data: JsValue = fetcher.getData(args.head)

    println(Json prettyPrint data)

    httpClient.close()
  }
}
