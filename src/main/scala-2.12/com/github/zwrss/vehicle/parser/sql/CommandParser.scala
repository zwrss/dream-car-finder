package com.github.zwrss.vehicle.parser.sql

import com.github.zwrss.otomoto.OtomotoVehicleFinder
import com.github.zwrss.otomoto.FakeOtomoto
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.command.Command
import com.github.zwrss.vehicle.command.Select
import com.github.zwrss.vehicle.command.FieldDescriptor
import com.github.zwrss.vehicle.command.ParameterDescriptor
import com.github.zwrss.vehicle.command.RownumDescriptor
import com.github.zwrss.vehicle.command.IdDescriptor
import com.github.zwrss.vehicle.command.CountDescriptor
import com.github.zwrss.vehicle.criterion.True
import com.github.zwrss.vehicle.finder.CompositeVehicleFinder
import com.github.zwrss.vehicle.finder.VehicleFinder

trait CommandParser extends AggregationParser with OrderingParser with CriterionParser {

  private def command: Parser[Command] = select

  private def select: Parser[Select] =
    ("select" ~> fields) ~ ("from" ~> source) ~ opt("where" ~> criterion) ~ opt("group" ~> "by" ~> aggregations) ~ opt("order" ~> "by" ~> orderingDescriptor) ~ opt("offset" ~> wholeNumber) ~ opt("limit" ~> wholeNumber) ^^ {
      case fields ~ source ~ query ~ grouping ~ ordering ~ offset ~ limit =>
        Select(fields, source, query getOrElse True, ordering, grouping, offset.map(_.toInt) getOrElse 0, limit.map(_.toInt) getOrElse 10)
    }

  private def fields: Parser[List[FieldDescriptor]] = allFields | exactFields

  private def allFields: Parser[List[FieldDescriptor]] = "*" ^^ { _ => List.empty }

  private def exactFields: Parser[List[FieldDescriptor]] = rep1sep(countField | iddesc | rownum | paramField, ",")

  private def countField: Parser[FieldDescriptor] = "count(" ~> ident <~ ")" ^^ { i => CountDescriptor(i) }

  private def rownum: Parser[FieldDescriptor] = "rownum" ^^ { _ => RownumDescriptor }

  private def iddesc: Parser[FieldDescriptor] = "id" ^^ { _ => IdDescriptor }

  private def paramField: Parser[FieldDescriptor] = ident ^^ { f =>
    ParameterDescriptor(VehicleParameter getParameter f)
  }

  private def source: Parser[VehicleFinder] = otomotoFinder | fakeFinder | compositeFinder

  private def otomotoFinder: Parser[VehicleFinder] = "otomoto" ^^ {
    _ => OtomotoVehicleFinder.getFinder
  }

  private def fakeFinder: Parser[VehicleFinder] = "fake" ^^ {
    _ => FakeOtomoto.getFinder
  }

  private def compositeFinder: Parser[VehicleFinder] = "composite" ^^ {
    _ => new CompositeVehicleFinder(Map("otomoto" -> OtomotoVehicleFinder.getFinder))
  }

  def toCommand(sql: String): Command = parseAll(command, sql) match {
    case Success(result, _)    => result
    case NoSuccess(message, _) => sys.error(s"Cannot parse [$sql]: " + message)
  }

}
