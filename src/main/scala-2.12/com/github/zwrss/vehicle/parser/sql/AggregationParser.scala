package com.github.zwrss.vehicle.parser.sql

import com.github.zwrss.vehicle.finder.Aggregation
import com.github.zwrss.vehicle.finder.AggregationResult
import com.github.zwrss.vehicle.finder.TermsAggregation
import com.github.zwrss.vehicle.parser.Parser

trait AggregationParser extends Parser {

  protected def aggregations: Parser[Aggregation[AggregationResult]] = rep1sep(ident, ",") ^^ {
    x =>
      x.dropRight(1).foldRight(TermsAggregation(x.last)) {
        case (t, agg) => TermsAggregation(t, List(agg))
      }
  }

  private def aggregation: Parser[Aggregation[AggregationResult]] = ident ^^ { x => TermsAggregation(s2p(x)) }

  def toAggregation(sql: String): Aggregation[AggregationResult] = parseAll(aggregations, sql) match {
    case Success(result, _)    => result
    case NoSuccess(message, _) => sys.error(s"Cannot parse [$sql]: " + message)
  }

}
