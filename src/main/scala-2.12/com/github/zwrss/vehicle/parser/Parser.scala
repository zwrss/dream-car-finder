package com.github.zwrss.vehicle.parser

import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue

import scala.util.parsing.combinator.JavaTokenParsers

trait Parser extends JavaTokenParsers {

  protected final def rep2sep[T](p: => Parser[T], q: => Parser[Any]): Parser[List[T]] = {
    p ~ rep1(q ~> p) ^^ { case x ~ y => x :: y }
  }

  protected final def strippedStringLiteral[T]: Parser[String] = stringLiteral.map { str =>
    str.stripPrefix("\"").stripSuffix("\"")
  }

  protected final def s2p(p: String): VehicleParameter[VehicleParameterValue] = {
    VehicleParameter getParameter p
  }

  protected final def s2pv(p: String, v: String): (VehicleParameter[VehicleParameterValue], VehicleParameterValue) = {
    val par = s2p(p)
    (par, par getValue v)
  }

}
