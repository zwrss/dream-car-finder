package com.github.zwrss.vehicle.parser.sql

import com.github.zwrss.vehicle.criterion.Criterion
import com.github.zwrss.vehicle.criterion.Equals
import com.github.zwrss.vehicle.criterion.In
import com.github.zwrss.vehicle.criterion.Range
import com.github.zwrss.vehicle.parser.Parser

trait CriterionParser extends Parser {

  protected def criterion: Parser[Criterion] = or | and | bracketedCriterion | simpleCriterion

  private def bracketedCriterion: Parser[Criterion] = "(" ~> unbracketedCriterion <~ ")"

  private def unbracketedCriterion: Parser[Criterion] = or | and | simpleCriterion

  private def simpleCriterion: Parser[Criterion] = equals | in | between | lte | gte

  //  private def and: Parser[Criterion] = (bracketedCriterion | simpleCriterion) ~ ("and" ~> criterion) ^^ { case c1 ~ c2 => c1 && c2 }

  private def and: Parser[Criterion] = (bracketedCriterion | simpleCriterion) ~ ("and" ~> (and | bracketedCriterion | simpleCriterion)) ^^ { case c1 ~ c2 => c1 && c2 }

  private def or: Parser[Criterion] = (and | bracketedCriterion | simpleCriterion) ~ ("or" ~> criterion) ^^ { case c1 ~ c2 => c1 || c2 }

  private def value: Parser[String] = strippedStringLiteral | decimalNumber | "true" | "false"

  private def equals: Parser[Equals] = ident ~ ("=" ~> value) ^^ {
    case p ~ v =>
      val (parameter, value) = s2pv(p, v)
      parameter === value
  }

  private def in: Parser[In] = (ident <~ "in") ~ ("(" ~> rep1sep(value, ",") <~ ")") ^^ {
    case p ~ v =>
      val parameter = s2p(p)
      parameter in (v map parameter.getValue)
  }

  private def between: Parser[Range] = (ident <~ "between") ~ value ~ ("and" ~> value) ^^ {
    case p ~ v1 ~ v2 =>
      val parameter = s2p(p)
      parameter.between(parameter getValue v1, parameter getValue v2)
  }

  private def lte: Parser[Range] = ident ~ ("<=" ~> value) ^^ {
    case p ~ v =>
      val (parameter, value) = s2pv(p, v)
      parameter <== value
  }

  private def gte: Parser[Range] = ident ~ (">=" ~> value) ^^ {
    case p ~ v =>
      val (parameter, value) = s2pv(p, v)
      parameter >== value
  }

  def toCriterion(sql: String): Criterion = parseAll(criterion, sql) match {
    case Success(result, _)    => result
    case NoSuccess(message, _) => sys.error(s"Cannot parse [$sql]: " + message)
  }

}
