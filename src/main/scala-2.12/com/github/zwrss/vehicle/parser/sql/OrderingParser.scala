package com.github.zwrss.vehicle.parser.sql

import com.github.zwrss.vehicle.command.OrderingDescriptor
import com.github.zwrss.vehicle.command.VehicleOrderingDescriptor
import com.github.zwrss.vehicle.command.CountDescriptor
import com.github.zwrss.vehicle.command.AggregatingOrdering
import com.github.zwrss.vehicle.ordering.VehicleOrdering

trait OrderingParser extends CriterionParser {

  protected def orderingDescriptor: Parser[OrderingDescriptor] = countOrdering | vehicleOrdering

  private def countOrdering: Parser[OrderingDescriptor] = ("count(" ~> ident <~ ")") ~ opt("asc") ~ opt("desc") ^^ {
    case a ~ _ ~ desc =>
      AggregatingOrdering(CountDescriptor(a), desc.isEmpty)
  }

  private def vehicleOrdering: Parser[OrderingDescriptor] = ordering.map(VehicleOrderingDescriptor.apply)

  private def ordering: Parser[VehicleOrdering] = innerOrdering ~ turn ^^ {
    case o ~ t =>
      if (t) o else o.reversed
  }

  private def innerOrdering: Parser[VehicleOrdering] = rep1sep(simpleOrdering, ",") ^^ {
    case o :: Nil => o
    case o :: r   => r.foldLeft(o) {
      case (agg, x) => agg and x
    }
  }

  private def simpleOrdering: Parser[VehicleOrdering] = ident ^^ { i => s2p(i).asc }

  private def turn: Parser[Boolean] = desc | asc

  private def asc: Parser[Boolean] = opt("asc") ^^ { _ => true }

  private def desc: Parser[Boolean] = "desc" ^^ { _ => false }

  def toOrdering(sql: String): OrderingDescriptor = parseAll(orderingDescriptor, sql) match {
    case Success(result, _)    => result
    case NoSuccess(message, _) => sys.error(s"Cannot parse [$sql]: " + message)
  }

}
