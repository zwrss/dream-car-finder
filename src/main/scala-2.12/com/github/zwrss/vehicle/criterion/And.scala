package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.Vehicle


case class And private[criterion](inner: List[Criterion]) extends Criterion {
  override def isSatisfiedBy(car: Vehicle): Boolean = inner forall (_ isSatisfiedBy car)
  override def toString: String = s"(${inner.mkString(" ∧ ")})"
}

object And {
  private[criterion] def apply(head: Criterion, tail: Criterion*): Criterion = tail.foldLeft(head) {
    case (True, criterion)                =>
      criterion
    case (criterion, True)                =>
      criterion
    case (And(criteria1), And(criteria2)) =>
      new And(criteria1 ++ criteria2)
    case (And(criteria), criterion)       =>
      new And(criteria :+ criterion)
    case (criterion, And(criteria))       =>
      new And(criterion :: criteria)
    case (criterion1, criterion2)         =>
      new And(List(criterion1, criterion2))
  }

  private[criterion] def flat(criteria: Iterable[Criterion]): Criterion = criteria.toList match {
    case head :: Nil  => head
    case head :: tail => apply(head, tail: _*)
  }
}
