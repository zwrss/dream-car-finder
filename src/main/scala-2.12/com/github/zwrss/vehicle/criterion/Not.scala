package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.Vehicle

case class Not private[criterion](inner: Criterion) extends Criterion {
  override def isSatisfiedBy(car: Vehicle): Boolean = !(inner isSatisfiedBy car)
  override def toString: String = s"¬$inner"
}


