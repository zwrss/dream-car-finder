package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue

case class Equals private[vehicle](parameter: VehicleParameter[VehicleParameterValue], value: VehicleParameterValue) extends SimpleCriterion {
  override def isSatisfiedBy(car: Vehicle): Boolean = car.parameter(parameter) contains value
  override def toString: String = s"$parameter ⇔ $value"
}
