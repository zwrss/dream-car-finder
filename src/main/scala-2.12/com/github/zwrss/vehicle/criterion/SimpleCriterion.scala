package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue

trait SimpleCriterion extends Criterion {
  def parameter: VehicleParameter[VehicleParameterValue]
}
