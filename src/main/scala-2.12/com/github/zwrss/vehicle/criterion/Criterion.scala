package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.Vehicle

trait Criterion {
  def isSatisfiedBy(car: Vehicle): Boolean

  final def &&(other: Criterion): Criterion = And(this, other)

  final def ||(other: Criterion): Criterion = Or(this, other)

  final def unary_! : Criterion = this match {
    case Not(x) => x
    case x      => Not(x)
  }
}

object Criterion {

  def flatten(criterion: Criterion): Criterion = criterion match {
    case And(criteria) =>
      val flattened = criteria.map(flatten).flatMap {
        case And(innerCriteria) => innerCriteria
        case c                  => List(c)
      }
      And(flattened)
    case Or(criteria)  =>
      val flattened = criteria.map(flatten).flatMap {
        case Or(innerCriteria) => innerCriteria
        case c                 => List(c)
      }
      Or(flattened)
    case c             => c
  }

  def in2or(criterion: Criterion): Criterion = criterion match {
    case In(parameter, values) =>
      if (values.size == 1) Equals(parameter, values.head)
      else Or(values map (Equals(parameter, _)): List[Criterion])
    case And(criteria)         =>
      And(criteria map in2or)
    case Or(criteria)          =>
      Or(criteria map in2or)
    case c                     => c
  }


  // and(A, or(B, C), or(D, E)) => or(and(A, B, D), and(A, B, E), and(A, C, D), and(A, C, E))
  def and2or(criterion: Criterion): Criterion = ???
//  {
//
//    criterion match {
//      case And(criteria) =>
//        val res = criteria.map(and2or).foldLeft(Set.empty: Set[Set[Criterion]]) {
//          case (agg, Or(c)) if agg.isEmpty  =>
//            c.map(Set(_))
//          case (agg, And(c)) if agg.isEmpty =>
//            Set(c)
//          case (agg, c) if agg.isEmpty      =>
//            Set(Set(c))
//          case (agg, Or(c))                 =>
//            for {
//              ct <- c
//              l <- agg
//            } yield {
//              l + ct
//            }
//          case (agg, And(c))                =>
//            agg.map(_ ++ c)
//          case (agg, c)                     =>
//            agg.map(_ + c)
//        }
//        Or flat (res map And.flat)
//      case Or(criteria)  =>
//        Or(criteria map and2or)
//      case c             => c
//    }
//
//  }

  def toOR(criterion: Criterion): Criterion = {
    val step1 = in2or(criterion)
    val step2 = and2or(step1)
    val res = flatten(step2)
    if (res == criterion) res
    else toOR(res)
  }

}