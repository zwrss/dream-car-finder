package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue
import com.github.zwrss.vehicle.parameter.simplevalue.NumberlikeValue

case class Range private[vehicle](
  parameter: VehicleParameter[VehicleParameterValue],
  from: Option[VehicleParameterValue] = None,
  to: Option[VehicleParameterValue] = None
) extends SimpleCriterion {

  private def isLess(a: VehicleParameterValue, b: VehicleParameterValue): Boolean = (a, b) match {
    case (x: NumberlikeValue, y: NumberlikeValue) =>
      x.asBigDecimal < y.asBigDecimal
    case _                                        =>
      false
  }

  override def isSatisfiedBy(car: Vehicle): Boolean = car.parameter(parameter) match {
    case Some(value) =>
      (from.isEmpty || !isLess(value, from.get)) && (to.isEmpty || !isLess(to.get, value))
    case _           =>
      false
  }

  override def toString: String = {
    (from, to) match {
      case (Some(a), Some(b)) =>
        s"$parameter ∈ <$a, $b>"
      case (Some(a), None)    =>
        s"$parameter ≥ $a"
      case (None, Some(b))    =>
        s"$parameter ≤ $b"
    }
  }

}
