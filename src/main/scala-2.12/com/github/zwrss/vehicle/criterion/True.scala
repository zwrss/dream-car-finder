package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.Vehicle


case object True extends Criterion {
  override def isSatisfiedBy(car: Vehicle): Boolean = true
  override def toString: String = s"true"
}


