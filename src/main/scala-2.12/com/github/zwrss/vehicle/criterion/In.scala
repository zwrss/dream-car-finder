package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue

case class In private[vehicle](parameter: VehicleParameter[VehicleParameterValue], values: List[VehicleParameterValue]) extends SimpleCriterion {
  override def isSatisfiedBy(car: Vehicle): Boolean = car.parameter(parameter) exists values.contains
  override def toString: String = s"$parameter ∈ { ${values.mkString(", ")} }"
}
