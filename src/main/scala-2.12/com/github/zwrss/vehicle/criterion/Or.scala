package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.Vehicle

case class Or private[criterion](inner: List[Criterion]) extends Criterion {
  override def isSatisfiedBy(car: Vehicle): Boolean = inner exists (_ isSatisfiedBy car)
  override def toString: String = s"(${inner.mkString(" ∨ ")})"
}

object Or {
  private[criterion] def apply(head: Criterion, tail: Criterion*): Criterion = tail.foldLeft(head) {
    case (True, _)                      =>
      True
    case (_, True)                      =>
      True
    case (Or(criteria1), Or(criteria2)) =>
      new Or(criteria1 ++ criteria2)
    case (Or(criteria), criterion)      =>
      new Or(criteria :+ criterion)
    case (criterion, Or(criteria))      =>
      new Or(criteria :+ criterion)
    case (criterion1, criterion2)       =>
      new Or(List(criterion1, criterion2))
  }

  private[criterion] def flat(criteria: Iterable[Criterion]): Criterion = criteria.toList match {
    case head :: Nil  => head
    case head :: tail => apply(head, tail: _*)
  }
}
