package com.github.zwrss.vehicle

import com.github.zwrss.util.MapFormat
import play.api.libs.json.Format
import play.api.libs.json.Json

case class Vehicle(id: String, parameters: Map[VehicleParameter[VehicleParameterValue], VehicleParameterValue]) {
  def parameter[T <: VehicleParameterValue](parameter: VehicleParameter[T]): Option[T] = {
    parameters.get(parameter).map(_.asInstanceOf[T])
  }
  override def toString: String = {
    val sortedParams = parameters.toSeq.map {
      case (key, value) => s" ${key.toString} = ${value.toString}"
    }.sorted.mkString("\n")
    s"""|Vehicle $id
        |$sortedParams""".stripMargin
  }
}

object Vehicle {

  type V = VehicleParameterValue
  type P = VehicleParameter[V]

  implicit def format: Format[Vehicle] = {
    implicit def mf = new MapFormat[P, V]
    Json.using[Json.WithDefaultValues].format
  }


}