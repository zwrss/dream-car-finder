package com.github.zwrss.vehicle.ordering

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue
import com.github.zwrss.vehicle.parameter.simplevalue.NumberlikeValue
import com.github.zwrss.vehicle.parameter.simplevalue.BooleanValue

import scala.math.Ordering

sealed trait VehicleOrdering {
  def ordering: Ordering[Vehicle]
  def and(ordering: VehicleOrdering): VehicleOrdering = {
    MultiOrdering(this, ordering)
  }
  def reversed: VehicleOrdering = DescOrdering(this)
}

case class DescOrdering(a: VehicleOrdering) extends VehicleOrdering {
  override def ordering: Ordering[Vehicle] = new Ordering[Vehicle] {
    override def compare(x: Vehicle, y: Vehicle): Int = -a.ordering.compare(x, y)
  }

  override def reversed: VehicleOrdering = a
}

case class SingleOrdering(parameter: VehicleParameter[VehicleParameterValue]) extends VehicleOrdering {
  override def ordering: Ordering[Vehicle] = new Ordering[Vehicle] {
    override def compare(x: Vehicle, y: Vehicle): Int = {
      (x parameter parameter, y parameter parameter) match {
        case (Some(a: NumberlikeValue), Some(b: NumberlikeValue)) =>
          if (a.asBigDecimal < b.asBigDecimal) -1
          else if (a.asBigDecimal > b.asBigDecimal) 1
          else 0
        case (Some(a: BooleanValue), Some(b: BooleanValue))       =>
          java.lang.Boolean.compare(a.value, b.value)
        case (None, Some(_))                                      => -1
        case (Some(_), None)                                      => 1
        case _                                                    => 0
      }
    }
  }
}

case class MultiOrdering(a: VehicleOrdering, b: VehicleOrdering) extends VehicleOrdering {
  override def ordering: Ordering[Vehicle] = new Ordering[Vehicle] {
    override def compare(x: Vehicle, y: Vehicle): Int = {
      val fromA = a.ordering.compare(x, y)
      if (fromA == 0) b.ordering.compare(x, y)
      else fromA
    }
  }
}