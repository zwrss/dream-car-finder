package com.github.zwrss.vehicle.parameter.simplevalue

import scala.util.Try

case class DecimalValue(value: BigDecimal) extends NumberlikeValue {
  override def asBigDecimal: BigDecimal = value
}

object DecimalValue extends SimpleParameterValueFactory[DecimalValue] {
  override def toValue(str: String): Option[DecimalValue] = {
    Try(DecimalValue(BigDecimal(str))).toOption
  }
}
