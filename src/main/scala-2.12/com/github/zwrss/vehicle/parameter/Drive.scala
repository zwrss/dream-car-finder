package com.github.zwrss.vehicle.parameter

import com.github.zwrss.vehicle.ListVehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue

sealed trait Drive extends VehicleParameterValue

object Drive extends ListVehicleParameter[Drive] {

  case object AllWheel extends Drive

  case object FrontWheel extends Drive

  case object RearWheel extends Drive

  protected val all: List[Drive] =
    List(AllWheel, FrontWheel, RearWheel)
}