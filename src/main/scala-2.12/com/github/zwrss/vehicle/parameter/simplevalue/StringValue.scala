package com.github.zwrss.vehicle.parameter.simplevalue

case class StringValue(value: String) extends SimpleParameterValue

object StringValue extends SimpleParameterValueFactory[StringValue] {
  override def toValue(str: String): Option[StringValue] = {
    Option(StringValue(str))
  }
}
