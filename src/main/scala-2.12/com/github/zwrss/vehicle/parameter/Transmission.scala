package com.github.zwrss.vehicle.parameter

import com.github.zwrss.vehicle.ListVehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue

sealed trait Transmission extends VehicleParameterValue

object Transmission extends ListVehicleParameter[Transmission] {

  case object Automatic extends Transmission

  case object ContinuouslyVariable extends Transmission

  case object Manual extends Transmission

  case object SemiAutomatic extends Transmission

  protected val all: List[Transmission] =
    List(Automatic, ContinuouslyVariable, Manual, SemiAutomatic)
}