package com.github.zwrss.vehicle.parameter.simplevalue

import scala.util.Try

case class IntValue(value: Int) extends NumberlikeValue {
  override def asBigDecimal: BigDecimal = value
}

object IntValue extends SimpleParameterValueFactory[IntValue] {
  override def toValue(str: String): Option[IntValue] = {
    Try(IntValue(str.toInt)).toOption
  }
}
