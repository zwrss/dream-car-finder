package com.github.zwrss.vehicle.parameter.simplevalue

trait NumberlikeValue extends SimpleParameterValue {
  def asBigDecimal: BigDecimal
}
