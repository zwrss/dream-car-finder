package com.github.zwrss.vehicle.parameter.simplevalue

import scala.util.Try

case class BooleanValue(value: Boolean) extends SimpleParameterValue

object BooleanValue extends SimpleParameterValueFactory[BooleanValue] {
  override def toValue(str: String): Option[BooleanValue] = {
    Try(BooleanValue(str.toBoolean)).toOption
  }
}
