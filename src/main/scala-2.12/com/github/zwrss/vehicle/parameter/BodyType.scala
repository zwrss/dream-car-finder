package com.github.zwrss.vehicle.parameter

import com.github.zwrss.vehicle.VehicleParameterValue
import com.github.zwrss.vehicle.ListVehicleParameter

sealed trait BodyType extends VehicleParameterValue

object BodyType extends ListVehicleParameter[BodyType] {

  case object Small extends BodyType

  case object Coupe extends BodyType

  case object Cabriolet extends BodyType

  case object Saloon extends BodyType

  case object Estate extends BodyType

  case object Van extends BodyType

  case object SUV extends BodyType

  protected val all: List[BodyType] = List(Small, Coupe, Cabriolet, Saloon, Estate, Van, SUV)
}
