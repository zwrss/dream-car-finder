package com.github.zwrss.vehicle.parameter

import com.github.zwrss.vehicle.SimpleVehicleParameter
import com.github.zwrss.vehicle.parameter.simplevalue.IntValue

object Mileage extends SimpleVehicleParameter(IntValue)
