package com.github.zwrss.vehicle.parameter

import com.github.zwrss.vehicle.ListVehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue

sealed trait Fuel extends VehicleParameterValue

object Fuel extends ListVehicleParameter[Fuel] {

  case object CNG extends Fuel

  case object Diesel extends Fuel

  case object Electric extends Fuel

  case object Hybrid extends Fuel

  case object LPG extends Fuel

  case object Petrol extends Fuel

  protected val all: List[Fuel] =
    List(CNG, Diesel, Electric, Hybrid, LPG, Petrol)
}