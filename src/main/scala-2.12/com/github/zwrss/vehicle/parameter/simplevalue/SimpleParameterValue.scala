package com.github.zwrss.vehicle.parameter.simplevalue

import com.github.zwrss.vehicle.VehicleParameterValue

trait SimpleParameterValue extends VehicleParameterValue {
  def value: Any

  final override def toString: String = value.toString
}

abstract class SimpleParameterValueFactory[V <: SimpleParameterValue] {
  def toValue(str: String): Option[V]
}
