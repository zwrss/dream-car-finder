package com.github.zwrss.vehicle.command

import com.github.zwrss.console.TablePrinter
import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.criterion.Criterion
import com.github.zwrss.vehicle.criterion.True
import com.github.zwrss.vehicle.finder.VehicleFinder
import com.github.zwrss.vehicle.finder.Aggregation
import com.github.zwrss.vehicle.finder.AggregationResult
import com.github.zwrss.vehicle.finder.TermsAggregation
import com.github.zwrss.vehicle.ordering.SingleOrdering
import com.github.zwrss.vehicle.ordering.DescOrdering

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.math.Ordering.StringOrdering
import scala.util.Try

case class Select(
  fields: List[FieldDescriptor],
  finder: VehicleFinder,
  criterion: Criterion = True,
  ordering: Option[OrderingDescriptor],
  grouping: Option[Aggregation[AggregationResult]],
  offset: Int = 0,
  size: Int = 10
) extends Command {

  private implicit def sync[T](future: Future[T]): T = Await.result(future, Duration.Inf)

  private def find(): String = {
    val vehicles: Seq[Vehicle] = finder.find(size, offset, criterion, ordering.map {
      case o: VehicleOrderingDescriptor => o.o
      case o                            => sys.error(s"Aggregation Ordering $o not supported on find")
    })

    val effectiveFields: Seq[SingleRowFieldDescriptor] = {
      if (fields.nonEmpty) fields.flatMap {
        case f: SingleRowFieldDescriptor => Option(f)
        case f                           => sys.error(s"Aggregation Field $f not supported on find")
      }
      else {
        List(RownumDescriptor, IdDescriptor) ++ vehicles.flatMap(_.parameters.keys).distinct.sortBy(_.toString).map(ParameterDescriptor)
      }
    }

    val headers = effectiveFields.map(_.toString).toList

    val rows: List[List[String]] = vehicles.zipWithIndex.map {
      case (vehicle, rownum) =>
        effectiveFields.map { parameter =>
          parameter.getValue(vehicle, rownum + 1)
        }.toList
    }.toList

    TablePrinter.format(headers, rows)
  }

  private def aggregate(): String = {

    assert(grouping.isDefined)

    val effectiveFields: Seq[AggregatingFieldDescriptor] = fields.map {
      case f: AggregatingFieldDescriptor                                   => f
      case f: ParameterDescriptor if grouping.get.parameter == f.parameter => TermDescriptor(f.parameter)
      case f                                                               => sys.error(s"Single Field $f not supported on aggregation")
    }

    val effectiveAgg: Option[Aggregation[AggregationResult]] = grouping orElse effectiveFields.collectFirst {
      case t: CountDescriptor => TermsAggregation(t.parameter)
    }

    val effectiveOrdering: Option[AggregatingOrdering] = ordering.map {
      case VehicleOrderingDescriptor(f: SingleOrdering) if f.parameter == grouping.get.parameter               =>
        AggregatingOrdering(TermDescriptor(f.parameter), true)
      case VehicleOrderingDescriptor(DescOrdering(f: SingleOrdering)) if f.parameter == grouping.get.parameter =>
        AggregatingOrdering(TermDescriptor(f.parameter), false)
      case f: AggregatingOrdering                                                                              => f
      case f                                                                                                   =>
        sys.error(s"Single Field Ordering $f not supported on aggregation")
    }

    if (effectiveFields.isEmpty || effectiveAgg.isEmpty) "" else {

      val aggregationsResult: Map[Aggregation[AggregationResult], AggregationResult] = finder.aggregate(criterion, effectiveAgg.toSeq)

      val aggregationResult = aggregationsResult.headOption.map(_._2)

      if (aggregationResult.isEmpty) "" else {

        val headers = effectiveFields.map(_.toString).toList

        val rows: List[List[String]] = {
          val allAggs: Seq[(AggregatingFieldDescriptor, Map[String, String])] = effectiveFields.map(f => f -> f.getValue(aggregationResult.get))
          val elements = allAggs.flatMap(_._2.keys)

          val rows: Seq[(String, Map[AggregatingFieldDescriptor, String])] = {
            val a = for {
              (field, elemMap) <- allAggs
              (elem, value) <- elemMap
            } yield {
              elem -> (field -> value)
            }
            a.groupBy(_._1).mapValues(_.map(_._2).toMap).toSeq
          }

          val orderedRows = effectiveOrdering match {
            case Some(o) =>
              val t = rows.sortBy(r => r._2.getOrElse(o.fieldDescriptor, " "))(new Ordering[String] {
                override def compare(x: String, y: String): Int = {
                  (Try(BigDecimal(x)).toOption, Try(BigDecimal(y)).toOption) match {
                    case (Some(a), Some(b)) => if (a < b) -1 else if (a > b) 1 else 0
                    case _                  => new StringOrdering {}.compare(x, y)
                  }
                }
              })
              if (o.ascending) t else t.reverse
            case _       => rows
          }

          orderedRows.toList.map {
            case (elem, map) =>
              effectiveFields.toList.map { f =>
                map.getOrElse(f, " ")
              }
          }
        }

        TablePrinter.format(headers, rows)

      }

    }
  }

  override def execute(): String = {
    if (grouping.isDefined || fields.exists(_.isInstanceOf[AggregatingFieldDescriptor])) aggregate() else find()
  }

}
