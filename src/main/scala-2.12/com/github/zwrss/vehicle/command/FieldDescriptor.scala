package com.github.zwrss.vehicle.command

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue
import com.github.zwrss.vehicle.finder.AggregationResult
import com.github.zwrss.vehicle.finder.TermsAggregationResult

sealed trait FieldDescriptor

sealed trait SingleRowFieldDescriptor extends FieldDescriptor {
  def getValue(vehicle: Vehicle, rownum: Int): String
}

sealed trait AggregatingFieldDescriptor extends FieldDescriptor {
  def getValue(aggregationResult: AggregationResult): Map[String, String]
}

case class ParameterDescriptor(parameter: VehicleParameter[VehicleParameterValue]) extends SingleRowFieldDescriptor {
  override def getValue(vehicle: Vehicle, rownum: Int): String = vehicle.parameter(parameter).map(_.toString).getOrElse(" ")

  override def toString: String = parameter.toString
}

case object RownumDescriptor extends SingleRowFieldDescriptor {
  override def getValue(vehicle: Vehicle, rownum: Int): String = rownum.toString

  override def toString: String = "rownum"
}

case object IdDescriptor extends SingleRowFieldDescriptor {
  override def getValue(vehicle: Vehicle, rownum: Int): String = vehicle.id

  override def toString: String = "ID"
}

case class CountDescriptor(parameter: VehicleParameter[VehicleParameterValue]) extends AggregatingFieldDescriptor {

  override def getValue(aggregationResult: AggregationResult): Map[String, String] = aggregationResult match {
    case t: TermsAggregationResult => t.values.map {
      case (x, c) => x.toString -> c.toString
    }
  }

  override def toString: String = s"count($parameter)"
}

case class TermDescriptor(parameter: VehicleParameter[VehicleParameterValue]) extends AggregatingFieldDescriptor {

  override def getValue(aggregationResult: AggregationResult): Map[String, String] = aggregationResult match {
    case t: TermsAggregationResult => t.values.map {
      case (x, _) => x.toString -> x.toString
    }
  }

  override def toString: String = parameter.toString
}