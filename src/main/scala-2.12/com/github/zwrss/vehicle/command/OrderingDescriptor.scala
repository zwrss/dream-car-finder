package com.github.zwrss.vehicle.command

import com.github.zwrss.vehicle.ordering.VehicleOrdering

trait OrderingDescriptor

case class VehicleOrderingDescriptor(o: VehicleOrdering) extends OrderingDescriptor

case class AggregatingOrdering(fieldDescriptor: AggregatingFieldDescriptor, ascending: Boolean) extends OrderingDescriptor