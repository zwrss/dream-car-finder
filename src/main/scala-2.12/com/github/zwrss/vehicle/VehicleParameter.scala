package com.github.zwrss.vehicle

import com.github.zwrss.util.MappingFormat
import com.github.zwrss.vehicle.criterion.Equals
import com.github.zwrss.vehicle.criterion.In
import com.github.zwrss.vehicle.criterion.Range
import com.github.zwrss.vehicle.ordering.VehicleOrdering
import com.github.zwrss.vehicle.ordering.SingleOrdering
import com.github.zwrss.vehicle.ordering.DescOrdering
import com.github.zwrss.vehicle.parameter.AccidentFree
import com.github.zwrss.vehicle.parameter.BodyType
import com.github.zwrss.vehicle.parameter.Broken
import com.github.zwrss.vehicle.parameter.Displacement
import com.github.zwrss.vehicle.parameter.Doors
import com.github.zwrss.vehicle.parameter.Drive
import com.github.zwrss.vehicle.parameter.Fuel
import com.github.zwrss.vehicle.parameter.Make
import com.github.zwrss.vehicle.parameter.Mileage
import com.github.zwrss.vehicle.parameter.Model
import com.github.zwrss.vehicle.parameter.Power
import com.github.zwrss.vehicle.parameter.Price
import com.github.zwrss.vehicle.parameter.ProductionYear
import com.github.zwrss.vehicle.parameter.Seats
import com.github.zwrss.vehicle.parameter.Transmission
import com.github.zwrss.vehicle.parameter.simplevalue.SimpleParameterValue
import com.github.zwrss.vehicle.parameter.simplevalue.SimpleParameterValueFactory
import play.api.libs.json.Format

sealed abstract class VehicleParameter[+T <: VehicleParameterValue] {
  override def toString: String = getClass.getSimpleName.stripSuffix("$")

  def toValue(string: String): Option[T]

  final def getValue(string: String): T = toValue(string).getOrElse(sys error s"$string is not a correct value for $toString")

  final def ===(value: VehicleParameterValue): Equals = Equals(this, value)

  final def in(values: Iterable[VehicleParameterValue]): In = In(this, values.toList)

  final def between(v1: VehicleParameterValue, v2: VehicleParameterValue): Range = Range(this, Option(v1), Option(v2))

  final def >==(value: VehicleParameterValue): Range = Range(this, from = Option(value))

  final def <==(value: VehicleParameterValue): Range = Range(this, to = Option(value))

  final def asc: VehicleOrdering = SingleOrdering(this)

  final def desc: VehicleOrdering = DescOrdering(SingleOrdering(this))

}

abstract class ListVehicleParameter[+T <: VehicleParameterValue] extends VehicleParameter[T] {
  protected def all: List[T]

  final override def toValue(string: String): Option[T] = all.find(_.toString == string)
}

abstract class SimpleVehicleParameter[+T <: SimpleParameterValue](factory: SimpleParameterValueFactory[T]) extends VehicleParameter[T] {
  def getFactory: SimpleParameterValueFactory[_ <: SimpleParameterValue] = factory
  final override def toValue(string: String): Option[T] = factory.toValue(string)
}

object VehicleParameter {

  val all: List[VehicleParameter[VehicleParameterValue]] =
    List(AccidentFree, BodyType, Broken, Displacement, Doors, Drive, Fuel, Make,
      Mileage, Model, Power, Price, ProductionYear, Seats, Transmission)

  def toParameter(string: String): Option[VehicleParameter[VehicleParameterValue]] = {
    all.collectFirst {
      case p if p.toString == string => p
    }
  }

  implicit def getParameter(string: String): VehicleParameter[VehicleParameterValue] = {
    toParameter(string).getOrElse(sys.error(s"No parameter registered for $string"))
  }

  implicit def getName(parameter: VehicleParameter[VehicleParameterValue]): String = parameter.toString

  type P = VehicleParameter[VehicleParameterValue]

  implicit def format: Format[P] = new MappingFormat[String, P](getParameter, _.toString)

}
