package com.github.zwrss.vehicle

import com.github.zwrss.util.MappingFormat
import play.api.libs.json.Format

trait VehicleParameterValue {
  override def toString: String = getClass.getSimpleName.stripSuffix("$")
}

object VehicleParameterValue {

  implicit def implicitFormat(implicit parameter: VehicleParameter[VehicleParameterValue]): Format[VehicleParameterValue] = {
    format(parameter)
  }

  implicit def format(parameter: VehicleParameter[VehicleParameterValue]): Format[VehicleParameterValue] = {
    new MappingFormat[String, VehicleParameterValue](parameter.getValue, _.toString)
  }

}