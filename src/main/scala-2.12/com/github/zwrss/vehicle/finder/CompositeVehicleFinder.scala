package com.github.zwrss.vehicle.finder

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.criterion.Criterion
import com.github.zwrss.vehicle.ordering.VehicleOrdering

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class CompositeVehicleFinder(components: Map[String, VehicleFinder]) extends VehicleFinder {

  private def getFinder(id: String): Option[VehicleFinder] = {
    id.split('_').headOption flatMap components.get
  }

  private def addPrefix(finderPrefix: String)(id: String): String = {
    s"${finderPrefix}_$id"
  }

  private def addIdPrefix(finderPrefix: String)(vehicle: Vehicle): Vehicle = {
    vehicle.copy(id = addPrefix(finderPrefix)(vehicle.id))
  }

  override def find(id: String): Future[Option[Vehicle]] = {
    getFinder(id) match {
      case Some(x) => x.find(id)
      case None    => components.foldLeft(Future successful Option.empty[Vehicle]) {
        case (current, (finderPrefix, finder)) =>
          current flatMap {
            case Some(v) => Future successful Some(v)
            case None    => finder.find(id) map (_ map addIdPrefix(finderPrefix))
          }
      }
    }
  }

  override def findIds(size: Int, offset: Int, query: Criterion, ordering: Option[VehicleOrdering]): Future[Seq[String]] = {
    find(size, offset, query, ordering) map (_ map (_.id))
  }

  override def find(size: Int, offset: Int, query: Criterion, ordering: Option[VehicleOrdering]): Future[Seq[Vehicle]] = {
    Future sequence components.map {
      case (finderPrefix, finder) => finder.find(size + offset, 0, query, ordering) map (_ map { vehicle =>
        vehicle.copy(id = finderPrefix + "_" + vehicle.id)
      })
    } map { shards =>
      val merged: Seq[Vehicle] = shards.toSeq.flatten
      val sorted: Seq[Vehicle] = if (ordering.isDefined) merged.sorted(ordering.get.ordering) else merged
      sorted.slice(offset, offset + size)
    }
  }

  private def mergeMaps[K, V](m1: Map[K, V], m2: Map[K, V])(f: (V, V) => V): Map[K, V] = ???

  private def mergeAggregations(a: AggregationResult, b: AggregationResult): AggregationResult = (a, b) match {
    case (l: TermsAggregationResult, r: TermsAggregationResult) if l.parameter == r.parameter =>
      TermsAggregationResult(
        l.parameter,
        l.missing + r.missing,
        mergeMaps(l.values, r.values)(_ + _)
      )
    case _                                                                                    =>
      a
  }

  override def aggregate(query: Criterion, aggregations: Seq[Aggregation[AggregationResult]]): Future[Aggregations] = {
    Future sequence components.map {
      case (_, finder) => finder.aggregate(query, aggregations)
    } map { shards =>
      aggregations.flatMap { aggregation =>
        shards.flatMap(_ get aggregation).foldLeft(Option.empty[AggregationResult]) {
          case (Some(a), b) => Option(mergeAggregations(a, b))
          case (_, b)       => Option(b)
        } map (aggregation -> _)
      }(scala.collection.breakOut)
    }
  }

}

object CompositeVehicleFinder {
  def apply(head: (String, VehicleFinder), tail: (String, VehicleFinder)*): CompositeVehicleFinder = {
    new CompositeVehicleFinder((Seq(head) ++ tail).toMap)
  }
}

