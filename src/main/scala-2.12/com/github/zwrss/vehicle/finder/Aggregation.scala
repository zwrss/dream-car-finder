package com.github.zwrss.vehicle.finder

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue

import scala.reflect._

abstract class Aggregation[+R <: AggregationResult : ClassTag] {

  def parameter: VehicleParameter[VehicleParameterValue]

  def calculate(cars: List[Vehicle]): R

}

case class TermsAggregation(parameter: VehicleParameter[VehicleParameterValue], sub: List[Aggregation[AggregationResult]] = List.empty)
  extends Aggregation[TermsAggregationResult] {

  override def calculate(cars: List[Vehicle]): TermsAggregationResult = {

    val valuesOpt: Map[Option[VehicleParameterValue], List[Vehicle]] = cars.groupBy(_.parameter(parameter))

    val values: Map[VehicleParameterValue, Int] = valuesOpt.collect {
      case (Some(p), v) => p -> v.size
    }

    val missing: Int = (valuesOpt get None) map (_.size) getOrElse 0

    TermsAggregationResult(parameter, missing, values)
  }

}

sealed trait AggregationResult {
  def parameter: VehicleParameter[VehicleParameterValue]
}

case class TermsAggregationResult(
  parameter: VehicleParameter[VehicleParameterValue],
  missing: Int,
  values: Map[VehicleParameterValue, Int]
) extends AggregationResult
