package com.github.zwrss.vehicle.finder

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.criterion.Criterion
import com.github.zwrss.vehicle.ordering.VehicleOrdering

import scala.concurrent.Future

object DummyVehicleFinder extends VehicleFinder {
  override def find(id: String): Future[Option[Vehicle]] = Future successful None

  override def findIds(size: Int, offset: Int, query: Criterion, ordering: Option[VehicleOrdering]): Future[Seq[String]] = {
    Future successful Seq.empty
  }

  override def aggregate(
    query: Criterion, aggregations: Seq[Aggregation[AggregationResult]]
  ): Future[Aggregations] = {
    Future successful aggregations.map(a => a -> a.calculate(List.empty)).toMap
  }
}
