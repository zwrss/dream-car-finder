package com.github.zwrss.vehicle.finder

import com.github.zwrss.cache.Cache
import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.criterion.Criterion
import com.github.zwrss.vehicle.ordering.VehicleOrdering

import scala.concurrent.Future

class CachingVehicleFinder(inner: VehicleFinder, cache: Cache[String, Option[Vehicle]]) extends VehicleFinder {

  final override def find(id: String): Future[Option[Vehicle]] = {
    cache.load(id)(inner find id)
  }

  final override def findIds(size: Int, offset: Int, query: Criterion, ordering: Option[VehicleOrdering]): Future[Seq[String]] = {
    inner.findIds(size, offset, query, ordering)
  }

  final override def aggregate(query: Criterion, aggregations: Seq[Aggregation[AggregationResult]]): Future[Aggregations] = {
    inner.aggregate(query, aggregations)
  }

  def clearCache(): Unit = {
    cache.clear()
  }

}
