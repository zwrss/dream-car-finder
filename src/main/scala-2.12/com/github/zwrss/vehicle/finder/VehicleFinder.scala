package com.github.zwrss.vehicle.finder

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.criterion.Criterion
import com.github.zwrss.vehicle.ordering.VehicleOrdering

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

trait VehicleFinder {

  type Aggregations = Map[Aggregation[AggregationResult], AggregationResult]

  def find(id: String): Future[Option[Vehicle]]

  def findIds(size: Int, offset: Int, query: Criterion, ordering: Option[VehicleOrdering]): Future[Seq[String]]

  def find(size: Int, offset: Int, query: Criterion, ordering: Option[VehicleOrdering]): Future[Seq[Vehicle]] = {
    findIds(size, offset, query, ordering) flatMap (ids => Future.sequence(ids map find).map(_.flatten))
  }

  def aggregate(query: Criterion, aggregations: Seq[Aggregation[AggregationResult]]): Future[Aggregations]

}