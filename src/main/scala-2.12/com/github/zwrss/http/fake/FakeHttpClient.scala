package com.github.zwrss.http.fake

import com.github.zwrss.http.HttpClient
import com.github.zwrss.http.HttpResponse

import scala.concurrent.Future

class FakeHttpClient(defaultResponse: HttpResponse, responses: Map[String, HttpResponse] = Map.empty) extends HttpClient {

  override def close(): Unit = {}

  override def get(url: String): Future[HttpResponse] = Future successful responses.getOrElse(url, defaultResponse)
}
