package com.github.zwrss.http.fake

import com.github.zwrss.http.HttpClient
import com.github.zwrss.http.HttpResponse

import scala.concurrent.Future

case object NoopHttpClient extends HttpClient {
  override def get(url: String): Future[HttpResponse] = ???
  override def close(): Unit = {}
}
