package com.github.zwrss.http.delaying


import com.github.zwrss.http.HttpClient
import com.github.zwrss.http.HttpResponse

import scala.concurrent.Future
import scala.concurrent.duration.Duration

class DelayingHttpClient(inner: HttpClient, minDelay: Duration) extends HttpClient {

  override def close(): Unit = inner.close()

  private var lastCall = 0l

  override def get(url: String): Future[HttpResponse] = synchronized {
    val ct = System.currentTimeMillis()
    val remainingDelay = lastCall + minDelay.toMillis - ct
    if (remainingDelay > 0) Thread.sleep(remainingDelay)
    lastCall = ct
    inner.get(url)
  }
}
