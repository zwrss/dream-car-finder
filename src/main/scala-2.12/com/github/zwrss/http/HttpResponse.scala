package com.github.zwrss.http

import com.github.zwrss.serializer.Deserializer
import com.github.zwrss.serializer.Serializer

abstract class HttpResponse {
  def getResponseBody: String
}

object HttpResponse {
  implicit def serializer: Serializer[HttpResponse] = new Serializer[HttpResponse] {
    override def serialize(obj: HttpResponse): String = obj.getResponseBody
  }

  implicit def deserializer: Deserializer[HttpResponse] = new Deserializer[HttpResponse] {
    override def deserialize(str: String): HttpResponse = new HttpResponse {
      override def getResponseBody: String = str
    }
  }
}
