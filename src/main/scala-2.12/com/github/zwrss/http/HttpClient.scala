package com.github.zwrss.http

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.Duration

trait HttpClient {
  def get(url: String): Future[HttpResponse]

  def getSync(url: String): HttpResponse = Await.result(get(url), Duration.Inf)

  def close(): Unit
}
