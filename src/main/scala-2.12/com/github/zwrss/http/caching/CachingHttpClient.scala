package com.github.zwrss.http.caching

import com.github.zwrss.cache.Cache
import com.github.zwrss.http.HttpClient
import com.github.zwrss.http.HttpResponse

import scala.concurrent.Future

class CachingHttpClient(client: HttpClient, cache: Cache[String, HttpResponse]) extends HttpClient {
  override def get(url: String): Future[HttpResponse] = cache.load(url)(client get url)
  override def close(): Unit = client.close()
}
