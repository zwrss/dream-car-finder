package com.github.zwrss.http.asynchttpclient

import java.util.concurrent.Executor

import scala.concurrent.ExecutionContext

// todo tego rzeczywiscie nigdzie nie ma? a moze to jakis podstep jest?
class ExecutionContextExecutor(executionContext: ExecutionContext) extends Executor {
  override def execute(command: Runnable): Unit = executionContext execute command
}

object ExecutionContextExecutor {
  implicit def toExecutor(executionContext: ExecutionContext): Executor = {
    new ExecutionContextExecutor(executionContext)
  }
}

