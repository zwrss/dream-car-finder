package com.github.zwrss.http.asynchttpclient

import com.github.zwrss.http.HttpResponse
import org.asynchttpclient.Response

class ResponseWrapper(response: Response) extends HttpResponse {
  override def getResponseBody: String = response.getResponseBody
}
