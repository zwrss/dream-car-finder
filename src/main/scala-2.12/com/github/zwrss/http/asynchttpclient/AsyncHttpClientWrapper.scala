package com.github.zwrss.http.asynchttpclient


import com.github.zwrss.http.HttpClient
import org.asynchttpclient.AsyncCompletionHandler
import org.asynchttpclient.AsyncHttpClient
import org.asynchttpclient.Response
import org.asynchttpclient.DefaultAsyncHttpClient

import scala.concurrent.Future
import scala.concurrent.Promise

class AsyncHttpClientWrapper(client: AsyncHttpClient) extends HttpClient {

  def close(): Unit = client.close()

  override def get(url: String): Future[ResponseWrapper] = {
    val promise = Promise.apply[ResponseWrapper]()
    client.prepareGet(url).execute(new AsyncCompletionHandler[ResponseWrapper] {

      override def onCompleted(response: Response): ResponseWrapper = {
        val wrapper = new ResponseWrapper(response)
        promise success wrapper
        wrapper
      }

      override def onThrowable(t: Throwable): Unit = {
        promise failure t
      }

    })
    promise.future
  }
}

object AsyncHttpClientWrapper {
  def default: AsyncHttpClientWrapper = new AsyncHttpClientWrapper(new DefaultAsyncHttpClient)
}