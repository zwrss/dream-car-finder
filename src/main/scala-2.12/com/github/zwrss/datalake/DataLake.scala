package com.github.zwrss.datalake

import java.io.File

import com.github.zwrss.cache.FileCache
import com.github.zwrss.console.TablePrinter
import com.github.zwrss.http.{HttpClient, HttpResponse}
import com.github.zwrss.http.asynchttpclient.AsyncHttpClientWrapper
import com.github.zwrss.http.caching.CachingHttpClient
import com.github.zwrss.http.fake.NoopHttpClient
import com.github.zwrss.memdb.MemDB
import com.github.zwrss.memdb.QueryResult
import com.github.zwrss.tibia._

import scala.io.StdIn

object DataLake {

  def noopClient: HttpClient = NoopHttpClient

  def asyncClient: HttpClient = AsyncHttpClientWrapper.default

  def cachingClient: HttpClient = new CachingHttpClient(asyncClient, new FileCache[String, HttpResponse]({
    val f = new File("cache/http")
    f.mkdirs()
    f
  }))

  lazy val httpClient: HttpClient = cachingClient

  private val dataSources: Seq[DataSource] = Seq(
    new CreaturesDataSource(httpClient),
    new CreaturesLootDataSource(httpClient),
    new HuntingAreaDataSource(httpClient),
    new ItemsDataSource(httpClient),
    new HousesDataSource("Refugia", httpClient),
  )

  private val db = new MemDB("db")

  private def printResult(result: QueryResult): Unit = {
    println(TablePrinter.format(result.headers, result.stringValues()))
  }

  private def fetch(name: Option[String]): Unit = name match {
    case Some(n) =>
      dataSources.find(_.name == n) match {
        case Some(d) =>
          println(s"Inserting rows to $n")
          d.insertRowsSQL foreach { i =>
            println(i)
            db.execute(i)
          }
        case None =>
          println(s"No source with name $n")
      }
    case None =>
      dataSources.foreach { d =>
        println(s"fetch_data_source ${d.name}")
      }
  }

  private def create(name: Option[String]): Unit = name match {
    case Some(n) =>
      dataSources.find(_.name == n) match {
        case Some(d) =>
          println(s"Creating table $n")
          db.execute(d.createTableSQL)
        case None =>
          println(s"No source with name $n")
      }
    case None =>
      dataSources.foreach { d =>
        println(s"new_data_source ${d.name}")
      }
  }

  def main(args: Array[String]): Unit = {
    try {

      var command = ""

      val queryCommands = Set("select", "show")

      while (command != "exit") {
        command = StdIn.readLine("Insert SQL command or type clear_caches, data_source_all, new_data_source, fetch_data_source or 'exit'\n")
        if (command != "exit") {
          val cmdHeader = command.split(' ').headOption.getOrElse("")
          try {
            if (queryCommands(cmdHeader)) printResult(db.query(command))
            else if (cmdHeader == "data_source_all") {
              dataSources.foreach { ds =>
                create(Option(ds.name))
                fetch(Option(ds.name))
              }
            }
            else if (cmdHeader == "new_data_source") create(command.split(' ').tail.headOption)
            else if (cmdHeader == "fetch_data_source") fetch(command.split(' ').tail.headOption)
            else if (cmdHeader == "clear_caches") dataSources.foreach(_.clearCache)
            else if (cmdHeader == "script") println(s"${db.execute(command)}")
            else println(s"Updated ${db.executeUpdate(command)} rows")
          } catch {
            case t: Throwable =>
              println(s"Cannot execute command: ${t.getMessage}")
          }
        }
      }
    } catch {
      case t: Throwable =>
        t.printStackTrace(System.err)
        System.exit(1)
    }

    System.exit(0)
  }
}
