package com.github.zwrss.datalake

trait DataSource {

  def name: String

  def createTableSQL: String

  def insertRowsSQL: Seq[String]

  def clearCache: Unit

}
