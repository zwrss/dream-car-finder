package com.github.zwrss.util

import play.api.libs.json.Format
import play.api.libs.json.JsError
import play.api.libs.json.JsObject
import play.api.libs.json.JsResult
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsValue

class MapFormat[K, V](implicit encodeKey: K => String, decodeKey: String => K, valueFormat: K => Format[V]) extends Format[Map[K, V]] {

  override def writes(o: Map[K, V]): JsValue = JsObject(o.toSeq.map {
    case (key, value) =>
      encodeKey(key) -> valueFormat(key).writes(value)
  })

  override def reads(json: JsValue): JsResult[Map[K, V]] = json match {
    case JsObject(fields) =>
      fields.foldLeft(JsSuccess(Map.empty): JsResult[Map[K, V]]) {
        case (e: JsError, _)                => e
        case (agg, (fieldName, fieldValue)) =>
          val key = decodeKey(fieldName)
          for {
            m <- agg
            value <- valueFormat(key).reads(fieldValue)
          } yield {
            m.updated(key, value)
          }
      }
    case _                =>
      JsError("not an object")
  }

}
