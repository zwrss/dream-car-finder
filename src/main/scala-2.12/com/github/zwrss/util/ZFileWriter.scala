package com.github.zwrss.util

import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter

import com.github.zwrss.serializer.Serializer

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ZFileWriter(file: File) {
  def write[T: Serializer](o: T): Future[T] = Future {
    val writer = new BufferedWriter(new FileWriter(file))
    try {
      writer.write(implicitly[Serializer[T]] serialize o)
      writer.flush()
    } finally {
      writer.close()
    }
    o
  }
}
