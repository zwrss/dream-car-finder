package com.github.zwrss.util

import java.util.UUID

object UID {
  def hash(key: String): String = UUID.fromString(key).toString
}
