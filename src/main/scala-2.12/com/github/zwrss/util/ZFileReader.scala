package com.github.zwrss.util

import java.io.File

import com.github.zwrss.serializer.Deserializer

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.Source


class ZFileReader(file: File) {
  def readIfExists[T: Deserializer]: Future[Option[T]] = Future {
    if (file.exists()) {
      val src = Source.fromFile(file)
      val serialized = try {
        src.mkString
      } finally {
        src.close()
      }
      val result: T = implicitly[Deserializer[T]].deserialize(serialized)
      Option(result)
    } else {
      None
    }
  }
}