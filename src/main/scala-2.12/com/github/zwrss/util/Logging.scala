package com.github.zwrss.util

import org.slf4j.LoggerFactory

trait Logging {
  private val _logger = LoggerFactory.getLogger(getClass)

  def warn(msg: => Any) = if (_logger.isWarnEnabled) _logger.warn(msg.toString)

  def warn(msg: => Any, t: => Throwable) = if (_logger.isWarnEnabled) _logger.warn(msg.toString, t)

  def error(msg: => Any) = if (_logger.isErrorEnabled) _logger.error(msg.toString)

  def error(msg: => Any, t: => Throwable) = if (_logger.isErrorEnabled) _logger.error(msg.toString, t)

  def info(msg: => Any) = if (_logger.isInfoEnabled) _logger.info(msg.toString)

  def debug(msg: => Any) = if (_logger.isDebugEnabled) _logger.debug(msg.toString)
}