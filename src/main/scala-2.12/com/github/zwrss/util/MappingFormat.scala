package com.github.zwrss.util

import play.api.libs.json.Format
import play.api.libs.json.JsResult
import play.api.libs.json.JsValue

class MappingFormat[L, H](toHigh: L => H, toLow: H => L)(implicit lowerFormat: Format[L]) extends Format[H] {
  override def writes(o: H): JsValue = lowerFormat.writes(toLow(o))
  override def reads(json: JsValue): JsResult[H] = lowerFormat.reads(json).map(toHigh)
}
