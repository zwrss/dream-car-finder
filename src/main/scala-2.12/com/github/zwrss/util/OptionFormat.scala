package com.github.zwrss.util

import play.api.libs.json.Format
import play.api.libs.json.JsPath

object OptionFormat {
  implicit def format[T: Format]: Format[Option[T]] = JsPath.formatNullable
}
