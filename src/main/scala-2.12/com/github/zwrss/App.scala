package com.github.zwrss

import java.io.File

import com.github.zwrss.cache.FileCache
import com.github.zwrss.http.HttpResponse
import com.github.zwrss.http.asynchttpclient.AsyncHttpClientWrapper
import com.github.zwrss.http.caching.CachingHttpClient
import com.github.zwrss.http.delaying.DelayingHttpClient
import com.github.zwrss.otomoto.OtomotoVehicleFinder
import com.github.zwrss.serializer.Serializer._
import com.github.zwrss.vehicle.parser.sql.CommandParser

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.Duration
import scala.concurrent.duration.DurationInt
import scala.io.StdIn

object App {

  private implicit def sync[T](future: Future[T]): T = Await.result(future, Duration.Inf)

  lazy val httpClient = new CachingHttpClient(
    new DelayingHttpClient(
      AsyncHttpClientWrapper.default,
      100.millis
    ),
    new FileCache[String, HttpResponse](new File("cache/http"))
  )

  lazy val commandParser: CommandParser = new CommandParser {}

  def main(args: Array[String]): Unit = {

    OtomotoVehicleFinder.initialize(httpClient)

    var input = ""

    println("Car Finder 0.1 ALPHA RELEASE")
    println("Use at your own risk!")
    println("")

    while (input != "exit") {
      println("")
      input = StdIn.readLine("> ")
      if (input != "exit") {
        try {
          println(commandParser.toCommand(input).execute())
        } catch {
          case t: Throwable =>
            println(s"Cannot execute command [$input]. Error [${t.getMessage}].")
        }
      }
    }

    httpClient.close()
  }

}
