package com.github.zwrss.memdb

import java.sql.Connection
import java.sql.DriverManager

class MemDB(id: String) {

  private val DB_DRIVER = "org.h2.Driver"
  private val DB_CONNECTION = s"jdbc:h2:mem:$id;DB_CLOSE_DELAY=-1"
  private val DB_USER = ""
  private val DB_PASSWORD = ""

  private def getDBConnection: Connection = {
    Class.forName(DB_DRIVER)
    DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD)
  }

  def executeUpdate(sql: String)(implicit connection: Connection = getDBConnection): Int = {
    val stmt = connection.prepareStatement(sql)
    val updatedRows = stmt.executeUpdate()
    stmt.close()
    updatedRows
  }

  def execute(sql: String)(implicit connection: Connection = getDBConnection): Unit = {
    val stmt = connection.prepareStatement(sql)
    val updatedRows = stmt.execute()
    stmt.close()
  }

  def query(sql: String)(implicit connection: Connection = getDBConnection): QueryResult = {
    val stmt = connection.prepareStatement(sql)
    val result = stmt.executeQuery()
    val meta = result.getMetaData
    val headers = (1 to meta.getColumnCount).map(column => meta.getColumnName(column)).toList
    var resultList = List.empty[List[Any]]
    while (result.next()) {

      val rowList:List[Any] = (1 to meta.getColumnCount).map { column =>
        result.getObject(column)
      }.toList

      resultList :+= rowList

    }
    stmt.close()
    QueryResult(headers, resultList)
  }

}
