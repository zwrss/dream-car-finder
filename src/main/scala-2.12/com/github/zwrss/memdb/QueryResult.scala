package com.github.zwrss.memdb

case class QueryResult(headers: List[String], values: List[List[Any]]) {
  def stringValues(converter: Any => String = QueryResult.simpleConverter): List[List[String]] = {
    values.map(_.map(converter))
  }
}

object QueryResult {
  def simpleConverter(any: Any): String = any match {
    case url: String if url.startsWith("http") => s" $url "
    case null => "null"
    case x => x.toString
  }
}
