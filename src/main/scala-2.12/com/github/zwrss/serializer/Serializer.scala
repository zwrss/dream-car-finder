package com.github.zwrss.serializer

import play.api.libs.json.Json
import play.api.libs.json.Reads
import play.api.libs.json.Writes

trait Serializer[T] {

  def serialize(obj: T): String

}

trait Deserializer[T] {

  def deserialize(str: String): T

}

object Serializer {

  implicit def writesSerializer[T: Writes]: Serializer[T] = new Serializer[T] {
    override def serialize(obj: T): String = Json prettyPrint (Json toJson obj)
  }

  implicit def readsDeserializer[T: Reads]: Deserializer[T] = new Deserializer[T] {
    override def deserialize(str: String): T = (Json parse str).as[T]
  }
}