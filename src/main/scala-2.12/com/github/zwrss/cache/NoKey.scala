package com.github.zwrss.cache

import com.github.zwrss.serializer.Deserializer
import com.github.zwrss.serializer.Serializer

import scala.concurrent.Future

trait NoKeyCache[V] {
  this: Cache[NoKey, V] =>

  def load(loader: => Future[V]): Future[V] = load(NoKey)(loader)

  def loadSync(loader: => V): V = loadSync(NoKey)(loader)

}

trait NoKey

case object NoKey extends NoKey {
  implicit def serializer: Serializer[NoKey] = _ => "NoKey"
  implicit def deserializer: Deserializer[NoKey] = _ => NoKey
}
