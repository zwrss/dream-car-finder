package com.github.zwrss.cache

import scala.concurrent.Await
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.duration.Duration

trait Cache[K, V] {

  def load(key: K)(loader: => Future[V]): Future[V]

  def loadSync(key: K)(loader: => V): V

  def clear(): Unit

  def clear(key: K): Unit

}

trait SyncCache[K, V] extends Cache[K, V] {

  final override def load(key: K)(loader: => Future[V]): Future[V] = Future {
    loadSync(key)(Await.result(loader, Duration.Inf))
  }(ExecutionContext.global)

}

trait AsyncCache[K, V] extends Cache[K, V] {

  def loadSync(key: K)(loader: => V): V = {
    Await.result(load(key)(Future(loader)(ExecutionContext.global)), Duration.Inf)
  }

}
