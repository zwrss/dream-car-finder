package com.github.zwrss.cache

import scala.concurrent.Future

class MultiCache[K, V](inner: List[Cache[K, V]]) extends Cache[K, V] {

  override def load(key: K)(loader: => Future[V]): Future[V] = {
    def l(caches: List[Cache[K, V]]): Future[V] = caches match {
      case head :: tail =>
        head.load(key)(l(tail))
      case _            =>
        loader
    }

    l(inner)
  }

  override def loadSync(key: K)(loader: => V): V = {
    def l(caches: List[Cache[K, V]]): V = caches match {
      case head :: tail =>
        head.loadSync(key)(l(tail))
      case _            =>
        loader
    }

    l(inner)
  }

  override def clear(): Unit = inner.foreach(_.clear())

  override def clear(key: K): Unit = inner.foreach(_ clear key)

}


object MultiCache {
  def create[K, V](caches: Cache[K, V]*): MultiCache[K, V] = new MultiCache(caches.toList)
}