package com.github.zwrss.cache

import java.io.File

import com.github.zwrss.serializer.Deserializer
import com.github.zwrss.serializer.Serializer
import com.github.zwrss.util.ZFileReader
import com.github.zwrss.util.ZFileWriter

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class FileCache[K: Serializer, V: Serializer : Deserializer](cacheDirectory: File, fileExtension: Option[String] = None) extends AsyncCache[K, V] {

  implicit def k2s(key: K): String = implicitly[Serializer[K]].serialize(key)

  private def ensuredCacheDirectory: File = {
    if (!cacheDirectory.exists()) cacheDirectory.mkdir()
    cacheDirectory
  }

  private def getFile(id: String): File = {
    val filename = id.replaceAll("[^A-Za-z0-9]", "_") + fileExtension.getOrElse("")
    new File(ensuredCacheDirectory, filename)
  }

  private def fromCache(id: String)(loader: => Future[V]): Future[V] = {
    val cacheFile = getFile(id)
    val reader = new ZFileReader(cacheFile)
    reader.readIfExists flatMap {
      case Some(o) => Future successful o
      case _       => loader flatMap (new ZFileWriter(cacheFile) write _)
    }
  }

  override def load(key: K)(loader: => Future[V]): Future[V] = {
    fromCache(key)(loader)
  }

  def clear(): Unit = {
    ensuredCacheDirectory.listFiles().foreach(_.delete())
  }

  override def clear(key: K): Unit = {
    getFile(key).delete()
  }
}
