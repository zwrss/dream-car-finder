package com.github.zwrss.cache

import scala.collection.concurrent.TrieMap
import scala.collection.immutable.ListSet

class MemoryCache[K, V](cacheCapacity: Int) extends SyncCache[K, V] {

  private val items: TrieMap[K, V] = TrieMap.empty

  private var lastAccess: ListSet[K] = ListSet.empty

  override def loadSync(key: K)(loader: => V): V = {
    lastAccess += key
    items.get(key) match {
      case Some(x) => x
      case None    =>
        synchronized {
          items.get(key) match {
            case Some(x) => x
            case None    =>
              val x: V = loader
              items.update(key, x)
              if (items.size > cacheCapacity) {
                lastAccess.take(items.size - cacheCapacity).foreach { toRemove =>
                  items.remove(toRemove)
                  lastAccess -= toRemove
                }
              }
              x
          }
        }
    }
  }


  override def clear(): Unit = {
    items.clear()
    lastAccess = ListSet.empty
  }

  override def clear(key: K): Unit = {
    items.remove(key)
    lastAccess -= key
  }

}