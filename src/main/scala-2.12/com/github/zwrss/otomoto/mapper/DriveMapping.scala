package com.github.zwrss.otomoto.mapper

object DriveMapping extends CsvMapping {
  override protected def resourceName: String = "otomoto-drive.csv"
}
