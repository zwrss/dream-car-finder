package com.github.zwrss.otomoto.mapper

object ModelMapping extends CsvMapping {
  override protected def resourceName: String = "otomoto-models.csv"
}
