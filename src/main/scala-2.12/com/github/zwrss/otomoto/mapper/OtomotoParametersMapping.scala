package com.github.zwrss.otomoto.mapper

import com.github.zwrss.html.HtmlLookup
import com.github.zwrss.vehicle.SimpleVehicleParameter
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue
import com.github.zwrss.vehicle.parameter.AccidentFree
import com.github.zwrss.vehicle.parameter.BodyType
import com.github.zwrss.vehicle.parameter.Broken
import com.github.zwrss.vehicle.parameter.Displacement
import com.github.zwrss.vehicle.parameter.Doors
import com.github.zwrss.vehicle.parameter.Drive
import com.github.zwrss.vehicle.parameter.Fuel
import com.github.zwrss.vehicle.parameter.Make
import com.github.zwrss.vehicle.parameter.Mileage
import com.github.zwrss.vehicle.parameter.Model
import com.github.zwrss.vehicle.parameter.Power
import com.github.zwrss.vehicle.parameter.ProductionYear
import com.github.zwrss.vehicle.parameter.Seats
import com.github.zwrss.vehicle.parameter.Transmission
import com.github.zwrss.vehicle.parameter.simplevalue.BooleanValue

import scala.util.Try

object OtomotoParametersMapping {

  private lazy val byCaption: Map[String, VehicleParameter[VehicleParameterValue]] = Map(
    "Marka pojazdu" -> Make,
    "Model pojazdu" -> Model,
    "Rok produkcji" -> ProductionYear,
    "Przebieg" -> Mileage,
    "Pojemność skokowa" -> Displacement,
    "Rodzaj paliwa" -> Fuel,
    "Moc" -> Power,
    "Skrzynia biegów" -> Transmission,
    "Napęd" -> Drive,
    "Typ" -> BodyType,
    "Liczba drzwi" -> Doors,
    "Liczba miejsc" -> Seats,
    "Bezwypadkowy" -> AccidentFree,
    "Uszkodzony" -> Broken
  )

  def toParameter(caption: String): Option[VehicleParameter[VehicleParameterValue]] = {
    byCaption.get(caption)
  }

  def toParameterValue(parameter: VehicleParameter[VehicleParameterValue], valueCaption: String): Option[VehicleParameterValue] = {
    parameter match {
      case BodyType                                                     =>
        BodyTypeMapping captionToValue valueCaption flatMap BodyType.toValue
      case Displacement                                                 =>
        Displacement toValue valueCaption.stripSuffix("cm3").filter(_.isDigit)
      case Drive                                                        =>
        DriveMapping captionToValue valueCaption flatMap Drive.toValue
      case Fuel                                                         =>
        FuelMapping captionToValue valueCaption flatMap Fuel.toValue
      case Make                                                         =>
        MakeMapping captionToValue valueCaption flatMap Make.toValue
      case Model                                                        =>
        ModelMapping captionToValue valueCaption flatMap Model.toValue
      case Mileage                                                      =>
        Mileage toValue valueCaption.filter(_.isDigit)
      case Power                                                        =>
        Power toValue valueCaption.filter(_.isDigit)
      case Transmission                                                 =>
        TransmissionMapping captionToValue valueCaption flatMap Transmission.toValue
      case p: SimpleVehicleParameter[_] if p.getFactory == BooleanValue =>
        val value = valueCaption match {
          case "Tak" => Option(true)
          case "Nie" => Option(false)
          case x     => Try(x.toBoolean).toOption
        }
        value map BooleanValue.apply
      case _                                                            =>
        parameter toValue valueCaption
    }
  }

  def urlParameter(parameter: VehicleParameter[VehicleParameterValue]): Seq[String] = ???

  def urlParameterValue(parameter: VehicleParameter[VehicleParameterValue], value: VehicleParameterValue): Seq[String] = {
    parameter match {
      case BodyType     =>
        BodyTypeMapping valueToUrl value.toString
      case Drive        =>
        DriveMapping valueToUrl value.toString
      case Fuel         =>
        FuelMapping valueToUrl value.toString
      case Make         =>
        MakeMapping valueToUrl value.toString
      case Model        =>
        ModelMapping valueToUrl value.toString
      case Transmission =>
        TransmissionMapping valueToUrl value.toString
      case _            =>
        Seq(value.toString)
    }
  }

  private implicit def toNode(s: String): HtmlLookup = HtmlLookup apply s

  private val carPage = "https://www.otomoto.pl/osobowe/"
  private val makePage = "https://www.otomoto.pl/ajax/jsdata/params/"

}