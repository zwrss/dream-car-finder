package com.github.zwrss.otomoto.mapper

object FuelMapping extends CsvMapping {
  override protected def resourceName: String = "otomoto-fuel.csv"
}
