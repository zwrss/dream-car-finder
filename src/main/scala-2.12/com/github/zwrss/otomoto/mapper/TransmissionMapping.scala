package com.github.zwrss.otomoto.mapper

object TransmissionMapping extends CsvMapping {
  override protected def resourceName: String = "otomoto-transmission.csv"
}
