package com.github.zwrss.otomoto.mapper

import scala.io.Source

abstract class CsvMapping {

  protected def resourceName: String

  private lazy val csv: Seq[(String, String, String)] = {
    val src = Source.fromResource(resourceName)
    try {
      src.getLines().toList.map { line =>
        line.split(";").map(_.trim).toList match {
          case value :: caption :: urlValue :: Nil => (value, caption, urlValue)
        }
      }
    } finally {
      src.close()
    }
  }

  def captionToValue(caption: String): Option[String] = csv.find(_._2 == caption).map(_._1)

  def valueToUrl(value: String): Seq[String] = csv.filter(_._1 == value).map(_._3)

}
