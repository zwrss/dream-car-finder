package com.github.zwrss.otomoto.mapper

object BodyTypeMapping extends CsvMapping {
  override protected def resourceName: String = "otomoto-bodytype.csv"
}
