package com.github.zwrss.otomoto.mapper

object MakeMapping extends CsvMapping {
  override protected def resourceName: String = "otomoto-makes.csv"
}
