package com.github.zwrss.otomoto

import com.github.zwrss.http.HttpClient
import com.github.zwrss.util.Logging
import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.criterion.Criterion
import com.github.zwrss.vehicle.finder.Aggregation
import com.github.zwrss.vehicle.finder.AggregationResult
import com.github.zwrss.vehicle.finder.VehicleFinder

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class OtomotoVehicleFinder(protected val httpClient: HttpClient) extends VehicleFinder with OtomotoVehicleSearching with Logging {

  private val mainPageUrl = "https://www.otomoto.pl"
  private val directUrl = s"$directUrlPrefix%s$directUrlSuffix"

  private def idToUrl(id: String): String = directUrl format id

  override def find(id: String): Future[Option[Vehicle]] = {
    httpClient get idToUrl(id) map { response =>
      try {
        Option(OtomotoExtractor extractVehicle response.getResponseBody)
      } catch {
        case t: Throwable =>
          error(s"Error while getting $id", t)
          None
      }
    }
  }

  override def aggregate(query: Criterion, aggregations: Seq[Aggregation[AggregationResult]]): Future[Aggregations] = {
    find(1000, 0, query, None).map { vehicles =>
      aggregations.map { aggregation =>
        aggregation -> aggregation.calculate(vehicles.toList)
      }(scala.collection.breakOut)
    }
  }

}

object OtomotoVehicleFinder {

  private var _finder: Option[OtomotoVehicleFinder] = None

  def getFinder: OtomotoVehicleFinder = _finder getOrElse sys.error("otomoto finder not yet initialized")

  def initialize(httpClient: HttpClient): Unit = synchronized {
    if (_finder.isDefined) sys.error("otomoto finder already initialized")
    _finder = Option(new OtomotoVehicleFinder(httpClient))
  }

}