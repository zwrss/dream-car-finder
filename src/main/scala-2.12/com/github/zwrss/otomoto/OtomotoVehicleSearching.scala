package com.github.zwrss.otomoto

import com.github.zwrss.html.HtmlLookup
import com.github.zwrss.http.HttpClient
import com.github.zwrss.otomoto.mapper.BodyTypeMapping
import com.github.zwrss.otomoto.mapper.FuelMapping
import com.github.zwrss.otomoto.mapper.MakeMapping
import com.github.zwrss.otomoto.mapper.ModelMapping
import com.github.zwrss.util.Logging
import com.github.zwrss.vehicle.criterion.And
import com.github.zwrss.vehicle.criterion.Criterion
import com.github.zwrss.vehicle.criterion.Equals
import com.github.zwrss.vehicle.criterion.In
import com.github.zwrss.vehicle.criterion.Or
import com.github.zwrss.vehicle.criterion.Range
import com.github.zwrss.vehicle.criterion.True
import com.github.zwrss.vehicle.ordering.VehicleOrdering
import com.github.zwrss.vehicle.parameter.BodyType
import com.github.zwrss.vehicle.parameter.Fuel
import com.github.zwrss.vehicle.parameter.Make
import com.github.zwrss.vehicle.parameter.Model
import com.github.zwrss.vehicle.parameter.Price
import com.github.zwrss.vehicle.parameter.ProductionYear

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait OtomotoVehicleSearching extends Logging {

  protected def httpClient: HttpClient

  //https://www.otomoto.pl/osobowe/renault/espace/od-2008/?search%5Bfilter_float_price%3Afrom%5D=10000&search%5Bfilter_float_price%3Ato%5D=30000&search%5Bfilter_float_year%3Ato%5D=2014&search%5Bfilter_float_mileage%3Afrom%5D=100000&search%5Bfilter_float_mileage%3Ato%5D=250000&search%5Bfilter_float_engine_capacity%3Afrom%5D=1000&search%5Bfilter_float_engine_capacity%3Ato%5D=5000&search%5Bfilter_float_engine_power%3Afrom%5D=100&search%5Bfilter_float_engine_power%3Ato%5D=300&search%5Bfilter_enum_fuel_type%5D%5B0%5D=petrol&search%5Bfilter_enum_gearbox%5D%5B0%5D=automatic&search%5Bfilter_enum_gearbox%5D%5B1%5D=manual&search%5Bfilter_enum_transmission%5D%5B0%5D=front-wheel&search%5Bcountry%5D=

  //https://www.otomoto.pl/osobowe/renault/espace/od-2008/?search[filter_float_price:from]=10000&search[filter_float_price:to]=30000&search[filter_float_year:to]=2014&search[filter_float_mileage:from]=100000&search[filter_float_mileage:to]=250000&search[filter_float_engine_capacity:from]=1000&search[filter_float_engine_capacity:to]=5000&search[filter_float_engine_power:from]=100&search[filter_float_engine_power:to]=300&search[filter_enum_fuel_type][0]=petrol&search[filter_enum_gearbox][0]=automatic&search[filter_enum_gearbox][1]=cvt&search[filter_enum_gearbox][2]=dual-clutch&search[filter_enum_gearbox][3]=manual&search[filter_enum_gearbox][4]=semi-automatic&search[filter_enum_transmission][0]=front-wheel&search[country]=

  //https://www.otomoto.pl/osobowe/renault/espace/od-2008/?search[filter_float_price:from]=10000&search[filter_float_price:to]=30000&search[filter_float_year:to]=2014&search[filter_float_mileage:from]=100000&search[filter_float_mileage:to]=250000&search[filter_float_engine_capacity:from]=1000&search[filter_float_engine_capacity:to]=5000&search[filter_float_engine_power:from]=100&search[filter_float_engine_power:to]=300&search[filter_enum_fuel_type][0]=petrol&search[filter_enum_gearbox][0]=automatic&search[filter_enum_gearbox][1]=manual&search[filter_enum_transmission][0]=front-wheel&search[country]=
  private val baseUrl = "https://www.otomoto.pl/osobowe/"

  protected val directUrlPrefix = "https://www.otomoto.pl/oferta/"
  protected val directUrlSuffix = ".html"

  protected implicit def toNode(s: String): HtmlLookup = HtmlLookup apply s

  private case class SearchUrl(page: Int = 1, make: Option[String] = None,
    model: Option[String] = None, bodyType: Seq[String] = Nil,
    yearFrom: Option[String] = None, enums: Map[String, Seq[String]] = Map.empty,
    floats: Map[String, (Option[String], Option[String])] = Map.empty) {

    private def pageNum: String = if (page != 1) s"page=$page" else ""

    def getUrl: String = {
      val btString: String = {
        if (bodyType.isEmpty) ""
        else {
          bodyType.sorted.mkString("--") + "/"
        }
      }
      baseUrl +
      make.map(_ + "/").getOrElse("") +
      model.map(_ + "/").getOrElse("") +
      btString +
      yearFrom.map("od-" + _ + "/").getOrElse("") + "?" +
      enums.map {
        case (key, values) =>
          values.zipWithIndex.map {
            case (value, idx) =>
              s"search[filter_enum_$key][$idx]=$value"
          }.mkString("&")
      }.mkString("&") +
      floats.map {
        case (key, (from, to)) =>
          val t = from.toList.map(x => s"search[filter_float_$key:from]=$x") ++ to.map(x => s"search[filter_float_$key:to]=$x")
          t.mkString("&")
      }.mkString("&") + pageNum
    }

    def withFloat(key: String, from: Option[String], to: Option[String]): SearchUrl = {
      copy(floats = floats.updated(key, (from, to)))
    }

    def withFloatTo(key: String, to: Option[String]): SearchUrl = {
      val (xf, _) = floats.getOrElse(key, None -> None)
      withFloat(key, xf, to)
    }

    def withFloatFrom(key: String, from: Option[String]): SearchUrl = {
      val (_, xt) = floats.getOrElse(key, None -> None)
      withFloat(key, from, xt)
    }

    def nextPage: SearchUrl = copy(page = page + 1)

    def withEnum(key: String, value: String): SearchUrl = {
      copy(enums = enums.updated(key, enums.getOrElse(key, Nil) :+ value))
    }

    def withEnum(key: String, value: Seq[String]): SearchUrl = {
      copy(enums = enums.updated(key, value))
    }
  }

  private def urlMappers: PartialFunction[Criterion, SearchUrl => SearchUrl] = {
    case True                            =>
      identity
    case Equals(Make, make)              =>
      _.copy(make = MakeMapping.valueToUrl(make.toString).headOption)
    case Equals(Model, model)            =>
      _.copy(model = ModelMapping.valueToUrl(model.toString).headOption)
    case Equals(BodyType, bodyType)      =>
      _.copy(bodyType = BodyTypeMapping.valueToUrl(bodyType.toString))
    case In(BodyType, bodyTypes)         =>
      _.copy(bodyType = bodyTypes.flatMap(t => BodyTypeMapping.valueToUrl(t.toString)))
    case Equals(ProductionYear, year)    =>
      _.copy(yearFrom = Option(year.toString)).withFloatTo("year", Option(year.toString))
    case Range(ProductionYear, from, to) =>
      m => m.copy(yearFrom = from.map(_.toString) orElse m.yearFrom).withFloatTo("year", to.map(_.toString) orElse m.floats.get("year").flatMap(_._2))
    case Equals(Fuel, fuel)              =>
      _.withEnum("fuel_type", FuelMapping.valueToUrl(fuel.toString))
    case Equals(Price, price)            =>
      m => m.withFloat("price", Option(price.toString), Option(price.toString))
    case Range(Price, from, to)          =>
      m => m.withFloat("price", from.map(_.toString), to.map(_.toString))
  }

  private def get(remaining: Int, url: SearchUrl): Future[Seq[String]] = {
    if (remaining <= 0) Future.successful(Seq.empty)
    else {
      httpClient.get(url.getUrl).map { response =>
        OtomotoExtractor.extractOfferLinks(response.getResponseBody) map { link =>
          (link stripSuffix directUrlSuffix) stripPrefix directUrlPrefix
        }
      }.flatMap { shard =>
        if (shard.isEmpty) Future.successful(shard)
        else get(remaining - shard.size, url.nextPage).map(shard ++ _)
      }
    }
  }

  private def find(size: Int, criterion: Criterion): Future[Seq[String]] = criterion match {
    case criterion if urlMappers.isDefinedAt(criterion)           =>
      get(size, urlMappers(criterion)(SearchUrl()))
    case And(criteria) if criteria.forall(urlMappers.isDefinedAt) =>
      val url = criteria.foldLeft(SearchUrl()) {
        case (u, c) =>
          urlMappers(c)(u)
      }
      get(size, url)
    case Or(criteria)                                             =>
      Future.sequence(criteria.map(find(size, _))).map(_.flatten)
    case _                                                        =>
      warn(s"Unsupported query detected in [$criterion].")
      Future.successful(Nil)
  }

  final def findIds(size: Int, offset: Int, query: Criterion, ordering: Option[VehicleOrdering]): Future[Seq[String]] = {

    find(size + offset, query).map(_.slice(offset, size + offset))

  }


}
