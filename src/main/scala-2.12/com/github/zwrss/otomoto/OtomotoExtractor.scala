package com.github.zwrss.otomoto

import com.github.zwrss.html.HtmlExtractor._
import com.github.zwrss.html.HtmlLookup
import com.github.zwrss.html.HtmlSelector._
import com.github.zwrss.otomoto.mapper.OtomotoParametersMapping
import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.VehicleParameter
import com.github.zwrss.vehicle.VehicleParameterValue
import com.github.zwrss.vehicle.parameter.Price
import com.github.zwrss.vehicle.parameter.simplevalue.DecimalValue

object OtomotoExtractor {

  private implicit def string2lookup(s: String): HtmlLookup = HtmlLookup.apply(s)

  def extractVehicle(html: HtmlLookup): Vehicle = {
    val parameters: Iterable[(VehicleParameter[VehicleParameterValue], VehicleParameterValue)] = {
      extractParamData(html) flatMap {
        case (paramName, paramValue) =>
          for {
            parameter <- OtomotoParametersMapping.toParameter(paramName)
            value <- OtomotoParametersMapping.toParameterValue(parameter, paramValue)
          } yield {
            parameter -> value
          }
      }
    }
    val id = extractId(html)
    val (price, currency) = extractPriceWithCurrency(html)
    val effectivePrice = DecimalValue(calculatePrice(price, currency))
    Vehicle(id, parameters.toMap.updated(Price, effectivePrice))
  }

  def extractMakes(html: String): List[(String, String)] = {
    html ?? hasAttribute("name", "search[filter_enum_make]") \\ and("option", not(hasClass("disabled"))) >> asLookup map { option =>
      val value = option > asAttributeValue("value")
      val caption = (option > asCleanValue).replaceAll(" *\\([0-9]+\\)", "")
      value -> caption
    }
  }

  def extractOfferLinks(html: String): List[String] = {
    html \\ hasClass("offer-title__link") >> asHref
  }

  def extractLastPage(html: String): Int = {
    val pager = html \\ hasClass("om-pager") \\ and("li", not(hasClass("next"))) last
    val page = pager \ "a" \ "span" >? asValue
    page map (_.toInt) getOrElse 1
  }

  private def extractId(page: HtmlLookup): String = {
    val linkNode = page \? "html" \ "head" \ and("link", hasAttribute("rel", "canonical"))
    val link = linkNode > asAttributeValue("href")
    link.stripPrefix("https://www.otomoto.pl/oferta/").stripSuffix(".html")
  }

  private def extractParamData(page: HtmlLookup): Map[String, String] = {
    val params = page \\ hasClass("offer-params")
    val m = params \\ hasClass("offer-params__item") >> asLookup map { node =>
      val key = node \ hasClass("offer-params__label") > asValue
      val value = node \ hasClass("offer-params__value") \? "a" > asValue
      key -> value
    }
    m.toMap
  }

  private def extractPriceWithCurrency(page: HtmlLookup): (String, String) = {
    val price: String = page ?? hasClass("offer-price__number") > asCleanValue
    val currency: String = page ?? hasClass("offer-price__currency") > asCleanValue
    price -> currency
  }

  private def calculatePrice(price: String, currency: String): BigDecimal = currency match {
    case "PLN" => BigDecimal(price.filter(_.isDigit))
  }

//  private val equipmentExtractor: Function[HtmlLookup, Set[String]] = {
//    _ \\ hasClass("offer-features__row") \\ hasClass("offer-features__item") >> asCleanValue toSet
//  }

}
