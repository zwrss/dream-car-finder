package com.github.zwrss.otomoto

import com.github.zwrss.vehicle.Vehicle
import com.github.zwrss.vehicle.criterion.Criterion
import com.github.zwrss.vehicle.criterion.True
import com.github.zwrss.vehicle.finder.Aggregation
import com.github.zwrss.vehicle.finder.AggregationResult
import com.github.zwrss.vehicle.finder.VehicleFinder
import com.github.zwrss.vehicle.ordering.VehicleOrdering

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.Duration

class FakeOtomoto(vehicles: List[Vehicle]) extends VehicleFinder {
  override def find(id: String): Future[Option[Vehicle]] = Future.successful(vehicles.find(_.id == id))


  override def findIds(size: Int, offset: Int, query: Criterion, ordering: Option[VehicleOrdering]): Future[Seq[String]] = {
    find(size, offset, query, ordering).map(_.map(_.id))
  }

  override def find(size: Int, offset: Int, query: Criterion, ordering: Option[VehicleOrdering]): Future[Seq[Vehicle]] = {
    val filtered = vehicles.filter(query.isSatisfiedBy)
    val sorted = ordering match {
      case Some(o) => filtered.sorted(o.ordering)
      case _       => filtered
    }
    Future successful sorted.slice(offset, offset + size)
  }

  override def aggregate(query: Criterion, aggregations: Seq[Aggregation[AggregationResult]]): Future[Aggregations] = {
    find(1000, 0, query, None).map { vehicles =>
      aggregations.map { aggregation =>
        aggregation -> aggregation.calculate(vehicles.toList)
      }(scala.collection.breakOut)
    }
  }
}

object FakeOtomoto {

  private implicit def sync[T](future: Future[T]): T = Await.result(future, Duration.Inf)

  val getFinder: VehicleFinder = {
    val vehicles: Seq[Vehicle] = OtomotoVehicleFinder.getFinder.find(1000, 0, True, None)
    new FakeOtomoto(vehicles.toList.distinct)
  }

}
