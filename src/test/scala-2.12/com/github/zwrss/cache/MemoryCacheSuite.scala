package com.github.zwrss.cache

import org.scalatest.FunSuite
import org.scalatest.Matchers

import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.Duration


class MemoryCacheSuite extends FunSuite with Matchers {

  class ExceptionOnSecondTry {
    private var called: Boolean = false
    def apply(): Future[Int] = synchronized {
      if (called) throw new RuntimeException("Called second time")
      else {
        called = true
        Future successful 1
      }
    }
  }

  test("capacity test") {

    val cache = new MemoryCache[String, Int](10)
    val elems: Seq[(String, ExceptionOnSecondTry)] = (1 to 11) map (i => i.toString -> new ExceptionOnSecondTry)

    elems.foreach {
      case (key, loader) =>
        Await.result(cache.load(key)(loader()), Duration.Inf) shouldBe 1
    }

    a[RuntimeException] should be thrownBy {
      elems.headOption match {
        case Some((key, loader)) =>
          Await.result(cache.load(key)(loader()), Duration.Inf)
      }
    }

  }


}
