package com.github.zwrss.cache

import org.scalatest.FunSuite
import org.scalatest.Matchers

import scala.collection.concurrent.TrieMap
import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.duration.Duration


class MultiCacheSuite extends FunSuite with Matchers {

  class SimpleCache extends SyncCache[String, String] {

    val cache: TrieMap[String, String] = TrieMap.empty

    def contains(key: String): Boolean = cache.contains(key)

    def add(key: String, value: String): Unit = {
      cache.update(key, value)
    }

    def clear(key: String): Unit = {
      cache.remove(key)
    }

    def clear(): Unit = {
      cache.clear()
    }

    override def loadSync(key: String)(loader: => String): String = {
      cache.getOrElseUpdate(key, loader)
    }
  }

  private def l(value: String): Future[String] = Future.successful(value)

  private def t(value: String): Future[String] = throw new RuntimeException(value)

  private def f(value: String): Future[String] = Future.failed(new RuntimeException(value))

  private def w[T](value: Future[T]): T = Await.result(value, Duration.Inf)

  test("simple test") {

    val c1 = new SimpleCache
    val c2 = new SimpleCache

    val cache = MultiCache.create(c1, c2)

    w(cache.load("foo")(l("foo")))

    c1.contains("foo") shouldBe true
    c2.contains("foo") shouldBe true

    c1.clear("foo")

    w(cache.load("foo")(t("foo"))) shouldBe "foo"

    c1.contains("foo") shouldBe true

    c2.clear("foo")

    w(cache.load("foo")(t("foo"))) shouldBe "foo"

    c2.contains("foo") shouldBe false

    c1.clear("foo")

    a[RuntimeException] shouldBe thrownBy {
      w(cache.load("foo")(f("foo")))
    }

    c1.contains("foo") shouldBe false
    c2.contains("foo") shouldBe false

  }


}
