package com.github.zwrss.html

import com.github.zwrss.html.HtmlExtractor._
import com.github.zwrss.html.HtmlSelector._
import org.scalatest.FunSuite
import org.scalatest.Matchers


// ?   - find first
// \   - find child
// ??  - find first recursive
// \\  - find recursive
// \?  - find child or this
// >>  - extract list
// >?  - extract single optionally
// >   - extract single

class HtmlNodeSuite extends FunSuite with Matchers {

  private val xmlString =
    """
      |<?xml version="1.0" encoding="UTF-8"?>
      |
      |<bookstore>
      |
      |  <book>
      |    <title lang="en">Harry Potter</title>
      |    <price>29.99</price>
      |  </book>
      |
      |  <book>
      |    <title lang="en">Learning XML</title>
      |    <price>39.95</price>
      |  </book>
      |
      |</bookstore>
    """.stripMargin

  private val xml = HtmlLookup apply xmlString


  test("find child") {

    xml \ "bookstore" shouldBe HtmlLookup.empty

    xml \ "book" shouldBe HtmlLookup.apply(
      """
        |  <book>
        |    <title lang="en">Harry Potter</title>
        |    <price>29.99</price>
        |  </book>
        |  <book>
        |    <title lang="en">Learning XML</title>
        |    <price>39.95</price>
        |  </book>
      """.stripMargin
    )

    xml \ "book" \ "title" shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
        |    <title lang="en">Learning XML</title>
      """.stripMargin
    )

    xml \ "book" \ "price" shouldBe HtmlLookup.apply(
      """
        |    <price>29.99</price>
        |    <price>39.95</price>
      """.stripMargin
    )

    xml \ "title" shouldBe HtmlLookup.empty

    xml \ "price" shouldBe HtmlLookup.empty

    xml \ "missingTag" shouldBe HtmlLookup.empty

    xml \ hasAttribute("lang", "en") shouldBe HtmlLookup.empty

    xml \ hasAttribute("lang", "pl") shouldBe HtmlLookup.empty

    xml \ hasAttribute("missingAttribute", "en") shouldBe HtmlLookup.empty

  }

  test("find first child") {

    xml ? "bookstore" shouldBe HtmlLookup.empty

    xml ? "book" shouldBe HtmlLookup.apply(
      """
        |  <book>
        |    <title lang="en">Harry Potter</title>
        |    <price>29.99</price>
        |  </book>
      """.stripMargin
    )

    xml ? "book" ? "title" shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
      """.stripMargin
    )

    xml ? "book" ? "price" shouldBe HtmlLookup.apply(
      """
        |    <price>29.99</price>
      """.stripMargin
    )

    xml ? "title" shouldBe HtmlLookup.empty

    xml ? "price" shouldBe HtmlLookup.empty

    xml ? "missingTag" shouldBe HtmlLookup.empty

    xml ? hasAttribute("lang", "en") shouldBe HtmlLookup.empty

    xml ? hasAttribute("lang", "pl") shouldBe HtmlLookup.empty

    xml ? hasAttribute("missingAttribute", "en") shouldBe HtmlLookup.empty

  }

  test("find recursive") {

    xml \\ or(hasAttribute("lang", "en"), "price") shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
        |    <price>29.99</price>
        |    <title lang="en">Learning XML</title>
        |    <price>39.95</price>
      """.stripMargin
    )

    xml \\ "bookstore" shouldBe HtmlLookup.empty

    xml \\ "book" shouldBe HtmlLookup.apply(
      """
        |  <book>
        |    <title lang="en">Harry Potter</title>
        |    <price>29.99</price>
        |  </book>
        |  <book>
        |    <title lang="en">Learning XML</title>
        |    <price>39.95</price>
        |  </book>
      """.stripMargin
    )

    xml \\ "book" \\ "title" shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
        |    <title lang="en">Learning XML</title>
      """.stripMargin
    )

    xml \\ "book" \\ "price" shouldBe HtmlLookup.apply(
      """
        |    <price>29.99</price>
        |    <price>39.95</price>
      """.stripMargin
    )

    xml \\ "title" shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
        |    <title lang="en">Learning XML</title>
      """.stripMargin
    )

    xml \\ "price" shouldBe HtmlLookup.apply(
      """
        |    <price>29.99</price>
        |    <price>39.95</price>
      """.stripMargin
    )

    xml \\ "missingTag" shouldBe HtmlLookup.empty

    xml \\ hasAttribute("lang", "en") shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
        |    <title lang="en">Learning XML</title>
      """.stripMargin
    )

    xml \\ hasAttribute("lang", "pl") shouldBe HtmlLookup.empty

    xml \\ hasAttribute("missingAttribute", "en") shouldBe HtmlLookup.empty

  }

  test("find first recursive") {

    xml ?? or(hasAttribute("lang", "en"), "price") shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
      """.stripMargin
    )

    xml ?? "bookstore" shouldBe HtmlLookup.empty

    xml ?? "book" shouldBe HtmlLookup.apply(
      """
        |  <book>
        |    <title lang="en">Harry Potter</title>
        |    <price>29.99</price>
        |  </book>
      """.stripMargin
    )

    xml ?? "book" ?? "title" shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
      """.stripMargin
    )

    xml ?? "book" ?? "price" shouldBe HtmlLookup.apply(
      """
        |    <price>29.99</price>
      """.stripMargin
    )

    xml ?? "title" shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
      """.stripMargin
    )

    xml ?? "price" shouldBe HtmlLookup.apply(
      """
        |    <price>29.99</price>
      """.stripMargin
    )

    xml ?? "missingTag" shouldBe HtmlLookup.empty

    xml ?? hasAttribute("lang", "en") shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
      """.stripMargin
    )

    xml ?? hasAttribute("lang", "pl") shouldBe HtmlLookup.empty

    xml ?? hasAttribute("missingAttribute", "en") shouldBe HtmlLookup.empty

  }

  test("find child or this") {

    xml \? "bookstore" shouldBe xml

    xml \? "book" shouldBe HtmlLookup.apply(
      """
        |  <book>
        |    <title lang="en">Harry Potter</title>
        |    <price>29.99</price>
        |  </book>
        |  <book>
        |    <title lang="en">Learning XML</title>
        |    <price>39.95</price>
        |  </book>
      """.stripMargin
    )

    xml \? "book" \? "title" shouldBe HtmlLookup.apply(
      """
        |    <title lang="en">Harry Potter</title>
        |    <title lang="en">Learning XML</title>
      """.stripMargin
    )

    xml \? "book" \? "price" shouldBe HtmlLookup.apply(
      """
        |    <price>29.99</price>
        |    <price>39.95</price>
      """.stripMargin
    )

    xml \? "book" \? "missingTag" shouldBe HtmlLookup.apply(
      """
        |  <book>
        |    <title lang="en">Harry Potter</title>
        |    <price>29.99</price>
        |  </book>
        |  <book>
        |    <title lang="en">Learning XML</title>
        |    <price>39.95</price>
        |  </book>
      """.stripMargin
    )

    xml \? "title" shouldBe xml

    xml \? "price" shouldBe xml

    xml \? "missingTag" shouldBe xml

    xml \? hasAttribute("lang", "en") shouldBe xml

    xml \? hasAttribute("lang", "pl") shouldBe xml

    xml \? hasAttribute("missingAttribute", "en") shouldBe xml

  }

  test("extract list") {
    xml \ "book" \ "title" >> asValue shouldBe List("Harry Potter", "Learning XML")

    xml \ "book" >> asValue map (_.replaceAllLiterally("\n", "").replaceAll(" +", " ")) shouldBe List(
      "<title lang=\"en\">Harry Potter</title> <price>29.99</price>",
      "<title lang=\"en\">Learning XML</title> <price>39.95</price>"
    )

    xml\ "book" >> asHtml map (_.replaceAllLiterally("\n", "").replaceAll(" +", " ")) shouldBe List(
      "<book> <title lang=\"en\">Harry Potter</title> <price>29.99</price> </book>",
      "<book> <title lang=\"en\">Learning XML</title> <price>39.95</price> </book>"
    )

    xml \ "book" >> asCleanValue shouldBe List(
      "Harry Potter 29.99",
      "Learning XML 39.95"
    )

    HtmlLookup.empty >> asValue shouldBe List()
    HtmlLookup.empty >> asHtml shouldBe List()
    HtmlLookup.empty >> asCleanValue shouldBe List()
  }

  test("Bad XML") {

    val badXml = HtmlLookup.apply(
      """|<root>
         | <tag1>
         |  <tag2>foo</tag2>
         | </tag1>
         | <badtag>
         | <tag3>
         |  <tag4>foo</tag4>
         |  bar
         | </tag3>
         |</root>
      """.stripMargin)

    badXml \ "tag1" \ "tag2" > asValue shouldBe "foo"

    badXml \ "tag3" \ "tag4" > asValue shouldBe "foo"

    badXml \ "tag3" > asCleanValue shouldBe "foo bar"

  }


}
