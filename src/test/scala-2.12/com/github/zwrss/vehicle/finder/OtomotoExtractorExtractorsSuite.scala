package com.github.zwrss.vehicle.finder

import com.github.zwrss.html.HtmlLookup
import com.github.zwrss.http.HttpResponse
import com.github.zwrss.http.fake.FakeHttpClient
import com.github.zwrss.otomoto.OtomotoExtractor
import com.github.zwrss.vehicle.parameter.BodyType
import com.github.zwrss.vehicle.parameter.Make
import com.github.zwrss.vehicle.parameter.Model
import com.github.zwrss.vehicle.parameter.Price
import com.github.zwrss.vehicle.parameter.simplevalue.DecimalValue
import com.github.zwrss.vehicle.parameter.simplevalue.StringValue
import org.scalatest.FlatSpec
import org.scalatest.Matchers

import scala.io.Source


class OtomotoExtractorExtractorsSuite extends FlatSpec with Matchers {

  implicit def toNode(s: String): HtmlLookup = HtmlLookup apply s

  private val fakeHttpClient = new FakeHttpClient(new HttpResponse {
    override def getResponseBody: String = ""
  })

  behavior of "OtomotoExtractor"

  it should "extract makes" in {
    val html = Source.fromResource("main_page.html").mkString
    val makes = OtomotoExtractor.extractMakes(html)
    println("MAKES:")
    makes.foreach(println)
    println("__MAKES")
    makes.size shouldBe 113
    makes should contain("mercedes-benz" -> "Mercedes-Benz")
    makes should contain("mclaren" -> "McLaren")
    makes should contain("ssangyong" -> "SsangYong")
  }

  it should "extract links from list view" in {
    val html = Source.fromResource("volkswagen_list.html").mkString
    val links: List[String] = OtomotoExtractor.extractOfferLinks(html)
    links.size shouldBe 32

    val lastPage: Int = OtomotoExtractor.extractLastPage(html)
    lastPage shouldBe 990
  }

  it should "extract links from empty list view" in {
    val html = Source.fromResource("asia.html").mkString
    val links: List[String] = OtomotoExtractor.extractOfferLinks(html)
    links.size shouldBe 1

    val lastPage: Int = OtomotoExtractor.extractLastPage(html)
    lastPage shouldBe 1
  }

  it should "extract car information" in {
    val html = Source.fromResource("golf_car.html").mkString
    val car = OtomotoExtractor.extractVehicle(html)
    car.parameters(BodyType) shouldBe BodyType.Estate
    car.parameters(Make) shouldBe StringValue("volkswagen")
    car.parameters(Model) shouldBe StringValue("golf")
    car.parameters(Price) shouldBe DecimalValue(BigDecimal(38900))
  }
}
