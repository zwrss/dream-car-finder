//package com.github.zwrss.car.otomoto
//
//import com.github.zwrss.http.asynchttpclient.AsyncHttpClientWrapper
//import org.scalatest.Matchers
//import org.scalatest.FlatSpec
//import org.scalatest.AsyncFlatSpec
//
//class OtomotoCrawlerSuite extends AsyncFlatSpec with Matchers {
//
//  val crawler = new OtomotoCrawler(AsyncHttpClientWrapper.default)
//
//  behavior of "OtomotoCrawler"
//
//  it should "crawl makes" in {
//    crawler.crawlMakes map { makes =>
//      makes.size shouldBe 113
//      makes should contain("mercedes-benz")
//      makes should contain("mclaren")
//      makes should contain("ssangyong")
//    }
//  }
//
//  it should "crawl links from list view" in {
//    crawler.crawlLinks("aston-martin") map { links =>
//      links.size shouldBe 41
//    }
//  }
//
//  it should "crawl car information" in {
//    crawler.crawlCar("https://www.otomoto.pl/oferta/volkswagen-golf-1-6-tdi-105km-trendline-salon-polska-serwis-aso-i-wl-bezwypadkowy-ID6zCFAV.html") map { car =>
//      car.parameters("Typ") shouldBe "Kombi"
//      car.parameters("Marka pojazdu") shouldBe "Volkswagen"
//      car.parameters("Model pojazdu") shouldBe "Golf"
//      car.parameters("Wersja") shouldBe "VII (2012-)"
//      car.equipment should contain("Czujniki parkowania tylne")
//      car.equipment should contain("Światła przeciwmgielne")
//      car.equipment should contain("Klimatyzacja manualna")
//    }
//  }
//
//}
