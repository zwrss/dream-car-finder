package com.github.zwrss.vehicle.parser.sql

import com.github.zwrss.vehicle.finder.TermsAggregation
import com.github.zwrss.vehicle.parameter.Make
import com.github.zwrss.vehicle.parameter.BodyType
import com.github.zwrss.vehicle.parameter.Model
import org.scalatest.FlatSpec
import org.scalatest.Matchers

class AggregationParserSuite extends FlatSpec with Matchers {

  private val aggregationParser = new AggregationParser {}

  import aggregationParser._

  behavior of "AggregationParser"

  it should "parse aggregations" in {
    toAggregation("Make") shouldBe TermsAggregation(Make)
    toAggregation("Make, Model") shouldBe TermsAggregation(Make, List(TermsAggregation(Model)))
    toAggregation("Make, Model, BodyType") shouldBe TermsAggregation(Make, List(TermsAggregation(Model, List(TermsAggregation(BodyType)))))
  }

}
