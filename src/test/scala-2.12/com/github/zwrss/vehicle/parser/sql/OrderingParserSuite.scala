package com.github.zwrss.vehicle.parser.sql

import com.github.zwrss.vehicle.parameter.Make
import com.github.zwrss.vehicle.parameter.BodyType
import com.github.zwrss.vehicle.parameter.Model
import org.scalatest.FlatSpec
import org.scalatest.Matchers

class OrderingParserSuite extends FlatSpec with Matchers {

  private val orderingParser = new OrderingParser {}

  import orderingParser._

  behavior of "OrderingParser"

  it should "parse ordering" in {
    toOrdering("Make") shouldBe Make.asc
    toOrdering("Make asc") shouldBe Make.asc
    toOrdering("Make desc") shouldBe Make.desc
    toOrdering("Make, Model, BodyType") shouldBe (Make.asc and Model.asc and BodyType.asc)
    toOrdering("Make, Model, BodyType asc") shouldBe (Make.asc and Model.asc and BodyType.asc)
    toOrdering("Make, Model, BodyType desc") shouldBe (Make.asc and Model.asc and BodyType.asc).reversed
  }

}
