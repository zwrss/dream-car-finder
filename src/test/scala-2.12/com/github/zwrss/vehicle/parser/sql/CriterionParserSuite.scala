package com.github.zwrss.vehicle.parser.sql

import com.github.zwrss.vehicle.criterion.Equals
import com.github.zwrss.vehicle.criterion.In
import com.github.zwrss.vehicle.criterion.Range
import com.github.zwrss.vehicle.parameter.BodyType
import com.github.zwrss.vehicle.parameter.BodyType.Small
import com.github.zwrss.vehicle.parameter.Drive
import com.github.zwrss.vehicle.parameter.Drive.FrontWheel
import com.github.zwrss.vehicle.parameter.Fuel
import com.github.zwrss.vehicle.parameter.Fuel.Diesel
import com.github.zwrss.vehicle.parameter.Fuel.Petrol
import com.github.zwrss.vehicle.parameter.Power
import com.github.zwrss.vehicle.parameter.Transmission
import com.github.zwrss.vehicle.parameter.Transmission.Automatic
import com.github.zwrss.vehicle.parameter.simplevalue.DecimalValue
import org.scalatest.FlatSpec
import org.scalatest.Matchers

class CriterionParserSuite extends FlatSpec with Matchers {

  private val criterionParser = new CriterionParser { }

  import criterionParser._

  behavior of "CriterionParser"

  it should "parse simple criteria" in {
    toCriterion("Fuel = \"Petrol\"") shouldBe Equals(Fuel, Petrol)
    toCriterion("Fuel in (\"Petrol\", \"Diesel\")") shouldBe In(Fuel, List(Petrol, Diesel))
    toCriterion("Power >= \"100\"") shouldBe Range(Power, from = Option(DecimalValue(100)))
    toCriterion("Power <= \"200\"") shouldBe Range(Power, to = Option(DecimalValue(200)))
    toCriterion("Power between \"100\" and \"200\"") shouldBe Range(Power, Option(DecimalValue(100)), Option(DecimalValue(200)))
  }

  it should "parse 'and' and 'or'" in {

    val A = Equals(BodyType, Small)
    val a = "BodyType = \"Small\""
    val B = Equals(Fuel, Petrol)
    val b = "Fuel = \"Petrol\""
    val C = Equals(Drive, FrontWheel)
    val c = "Drive = \"FrontWheel\""
    val D = Equals(Transmission, Automatic)
    val d = "Transmission = \"Automatic\""

    toCriterion(s"$a and $b") shouldBe A && B

    toCriterion(s"$a or $b") shouldBe A || B

    A && B || C shouldBe (A && B) || C // just in case
    toCriterion(s"$a and $b or $c") shouldBe A && B || C

    toCriterion(s"$a and ($b or $c)") shouldBe A && (B || C)

    toCriterion(s"($a and $b) or $c") shouldBe (A && B) || C

    A || B && C shouldBe A || (B && C) // just in case
    toCriterion(s"$a or $b and $c") shouldBe A || B && C

    toCriterion(s"$a or ($b and $c)") shouldBe A || (B && C)

    toCriterion(s"($a or $b) and $c") shouldBe (A || B) && C

    A && B || C && D shouldBe (A && B) || (C && D) // just in case
    toCriterion(s"$a and $b or $c and $d") shouldBe A && B || C && D

    A && (B || C) && D shouldBe A && ((B || C) && D) // just in case
    toCriterion(s"$a and ($b or $c) and $d") shouldBe A && (B || C) && D

  }

}
