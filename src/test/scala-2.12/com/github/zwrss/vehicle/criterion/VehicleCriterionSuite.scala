package com.github.zwrss.vehicle.criterion

import com.github.zwrss.vehicle.parameter.Fuel
import com.github.zwrss.vehicle.parameter.Fuel.CNG
import com.github.zwrss.vehicle.parameter.Fuel.Diesel
import com.github.zwrss.vehicle.parameter.Fuel.Electric
import com.github.zwrss.vehicle.parameter.Fuel.Hybrid
import com.github.zwrss.vehicle.parameter.Fuel.LPG
import com.github.zwrss.vehicle.parameter.Fuel.Petrol
import org.scalatest.FlatSpec
import org.scalatest.Matchers

class VehicleCriterionSuite extends FlatSpec with Matchers {

  val A = Fuel === Petrol
  val B = Fuel === LPG
  val C = Fuel === Hybrid
  val D = Fuel === CNG
  val E = Fuel === Electric
  val F = Fuel === Diesel
  val G = Fuel in Set(Petrol, LPG)

  behavior of "Criteria"

  it should "and is commutative" in {
    A && B shouldBe B && A
    (A && (B && C)) shouldBe (C && (A && B))
  }

  it should "or is commutative" in {
    A || B shouldBe B || A
    (A || (B || C)) shouldBe (C || (A || B))
  }

  it should "get flatten correctly" in {

    Criterion.flatten(And(And(A, B), C)) shouldBe And(A, B, C)
    Criterion.flatten(Or(Or(A, B), C)) shouldBe Or(A, B, C)
    Criterion.flatten(And(Or(A, B), C)) shouldBe And(Or(A, B), C)
    Criterion.flatten(And(Or(And(A, B), C), D)) shouldBe And(Or(And(A, B), C), D)
    Criterion.flatten(And(And(And(A, B), C), D)) shouldBe And(A, B, C, D)

  }

  it should "change in to or" in {

    Criterion.in2or(G) shouldBe A || B

    Criterion.in2or(G && C) shouldBe (A || B) && C

  }

  it should "change and to or" in {

    Criterion.and2or(A && B) shouldBe A && B
    Criterion.and2or(A && (B || C)) shouldBe (A && B) || (A && C)
    Criterion.and2or((A || B) && C) shouldBe (A && C) || (B && C)
    Criterion.and2or((A || B) && (C || D)) shouldBe (A && C) || (B && C)  || (A && D)|| (B && D)

  }

  it should "be transformed into or" in {

    val a = Criterion.toOR(
      And(
        A,
        B,
        And(
          C,
          D,
          Or(E, F),
          Or(A, G)
        )
      )
    )

    // => A && B && C && D && (E || F) && (A || G)
    // => A && B && C && D && (E || F) && (A || A || B)
    // => A && B && C && D && (E || F) && (A || B)
    // => (A && B && C && D && E && (A || B)) || (A && B && C && D && F && (A || B))
    // => (A && B && C && D && E && A) || (A && B && C && D && E && B) || (A && B && C && D && F && (A || B))
    // => (A && B && C && D && E && A) || (A && B && C && D && E && B) || (A && B && C && D && F && A) || (A && B && C && D && F && B)
    // => (A && B && C && D && E) || (A && B && C && D && E) || (A && B && C && D && F) || (A && B && C && D && F)

    val b = (A && B && C && D && E) || (A && B && C && D && E) || (A && B && C && D && F) || (A && B && C && D && F)

    a shouldBe b

  }

}
